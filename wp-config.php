<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache





/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'jomplan_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'jomplan');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'wdyqmwyjhrkgvueliwcwubnmetd5dkdzolmhtldef02jnbegrj7a5ixlygot6tey');
define('SECURE_AUTH_KEY',  'swklbl3e7jzr5vesam48ubco3oho1gkmll7sofhf0sigolqjeuwzvqezofmbghqu');
define('LOGGED_IN_KEY',    'r9qghi2haikcnkmk0lepbj3tccm3d7pddp1zwdfkslvntnnbqz1tyjo6oiemz7mz');
define('NONCE_KEY',        '6litsxanydmkwwvce3rclkxxqapcnjbysyw94xtkifra55avkpuzap4kedlrobwi');
define('AUTH_SALT',        'qfh1dx2n6wzvd9fck7haszhrqzcoqrwwbgvlxkvvvwy7jxu7ciywjepytybtojql');
define('SECURE_AUTH_SALT', 'sygfdkwdbuqkwrhgmmnhtl7msdckznxgdzrq4b2zc9ogcixgnggoaqpnvriqbka3');
define('LOGGED_IN_SALT',   'iowavlhgxppmfgzij580oi2wt72umx0khcbnrgtlcy3rubj5p1cmoaegugnfyrhl');
define('NONCE_SALT',       'karn9ekw51xxcly60slewzsoyhnq4dbpyp27kue8vaxx8js7rqwqmqlznk5g8qi9');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpny_';


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
