<?php
include('include/config.php');
if ($con) {
    if (($_POST["tour_id"] != "") && ($_POST["profile_id"] != "") && ($_POST["flights"] != "") && ($_POST["travel_from"] != "") && ($_POST["travel_to"] != "") && ($_POST["depature_date"] != "") && ($_POST["return_date"] != "") && ($_POST["payment_status"] != "") && ($_POST["tour_price_per_person"] != "") && ($_POST["tour_total_price"] != "")) {

        $tour_id = $_POST["tour_id"];
        $profile_id = $_POST["profile_id"];
        $flights = $_POST["flights"]; // 1 = One Way, 2 = Round Trip
        $travel_from = $_POST["travel_from"];
        $travel_to = $_POST["travel_to"];
        $depature_date = $_POST["depature_date"]; // Date = YYYY-mm-dd
        $return_date = $_POST["return_date"]; // Date = YYYY-mm-dd
        $adults = $_POST["adults"];
        $teens = $_POST["teens"];
        $childrens = $_POST["childrens"];
        $infants = $_POST["infants"];
        $tour_price_per_person = $_POST["tour_price_per_person"];
        $tour_total_price = $_POST["tour_total_price"];
        $payment_status = $_POST["payment_status"];

        if($flights == 1){
            $flights_trip = "One Way";
        } else {
            $flights_trip = "Round Trip";
        }

        if($adults != "" || $adults != 0 || $teens != "" || $teens != 0 || $childrens != "" || $childrens != 0 || $infants != "" || $infants != 0){

            $query = mysqli_query($con, "insert into checkout set tour_id = '".$tour_id."', profile_id = '".$profile_id."', flights = '".$flights."', travel_from = '".$travel_from."', travel_to = '".$travel_to."', depature_date = '".$depature_date."', return_date = '".$return_date."', adults = '".$adults."', teens = '".$teens."', childrens = '".$childrens."', infants = '".$infants."', payment_status = '".$payment_status."' ");

            if ($query) {

                $last_id = mysqli_insert_id($con);    

                $reference_no = 100000 + $last_id;
                $update_query = mysqli_query($con, "update checkout set reference_no = '".$reference_no."' where id = '".$last_id."' ");

                if( $payment_status == 1){
                    $get_user_query = mysqli_query($con, "select * from userdetails WHERE profile_id = '".$profile_id."' ");
                    $get_user_res = mysqli_fetch_array($get_user_query);

                    $get_tour_query = mysqli_query($con, "select * from tour_details WHERE id = '".$tour_id."' ");
                    $get_tour_res = mysqli_fetch_array($get_tour_query);

                    $tour_name = $get_tour_res['tour_name'];                    
                    $from = "admin@jomplan.com";
                    $to = $get_user_res['email'];
                    $user_name = $get_user_res['first_name'];

                    $subject = "Jomplan - Your Tour(".$tour_name.") Booking Confirmed.";
                    $headers = "From: Jomplan - Smarter Way To Travel<" . $from . "> \r\n";
                    $headers .= "Return-Path: " . $from . " \r\n";
                    $headers .= "Reply-To: " . $from . " \r\n";
                    $headers .= "MIME-Version: 1.0\r\n";
                    $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
                    $headers .= "X-Priority: 3\r\n";
                    $headers .= "X-Mailer: PHP" . phpversion() . "\r\n";

                    $message = "<html>"
                            . "<head></head>"
                            . "<body>"
                            . "<h3>Hello " . $user_name . ",</h3><br>Your tour(<b>".$tour_name."</b>) booking confirmed, your booking reference number <b>".$reference_no."</b>. Please find the following details:<br><br>"
                            . "<table cellpadding='5' cellspacing='0' border='1' width='80%'>
                                <tr><th style='text-align:left'>Tour Name</th><td>".$tour_name."</td></tr>
                                <tr><th style='text-align:left'>Flights</th><td>".$flights_trip."</td></tr>
                                <tr><th style='text-align:left'>From</th><td>".$travel_from."</td></tr>
                                <tr><th style='text-align:left'>To</th><td>".$travel_to."</td></tr>
                                <tr><th style='text-align:left'>Depature Date</th><td>".date('M d, Y', strtotime($depature_date))."</td></tr>
                                <tr><th style='text-align:left'>Return Date</th><td>".date('M d, Y', strtotime($return_date))."</td></tr>
                                <tr><th style='text-align:left'>Adults</th><td>".$adults."</td></tr>
                                <tr><th style='text-align:left'>Teens</th><td>".$teens."</td></tr>
                                <tr><th style='text-align:left'>Childrens</th><td>".$childrens."</td></tr>
                                <tr><th style='text-align:left'>Infants</th><td>".$infants."</td></tr>
                                <tr><th style='text-align:left'>Tour Price/Person  </th><td>$".$tour_price_per_person."</td></tr>
                                <tr><th style='text-align:left'>Total Price </th><td><b>$".$tour_total_price."</b></td></tr>
                                </table>"
                            . "<br><br><p>Regards,<br>Jomplan - Smarter Way To Travel</p>"
                            . "</body>"
                            . "</html>";
                 
                    mail($to, $subject, $message, $headers);
                }


                $json = array("status" => 1, "msg" => "Payment Successful, We will send confirmation mail soon.");
                header('Content-type: application/json');
                echo json_encode($json);
            } else {
                $json = array("status" => 0, "msg" => "Error Occured");
                header('Content-type: application/json');
                echo json_encode($json);
            }

        } else {

            $json = array("status" => 0, "msg" => "Any one correct value enter in travellers field.");
            header('Content-type: application/json');
            echo json_encode($json);

        }

    } else {
        $json = array("status" => 0, "msg" => "Parameter(s) Missing!");
        header('Content-type: application/json');
        echo json_encode($json);
    }
} else {

    $json = array("status" => 0, "msg" => "Network Error");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>