<?php
/**
 * travelWP functions and definitions.
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package travelWP
 */

// Constants: Folder directories/uri's
define( 'TRAVELWP_THEME_DIR', trailingslashit( get_template_directory() ) );
define( 'TRAVELWP_THEME_URI', trailingslashit( get_template_directory_uri() ) );

/**
 * Theme Includes
 */

require_once TRAVELWP_THEME_DIR . '/inc/init.php';

//remove_action( "redux/extensions/travelwp_theme_options/before", 'physcode_custom_extension_loader', 0 );



// ============ Passport Information Start =====================

/**
 * Add the field to the checkout page
 */
add_action('woocommerce_after_order_notes', 'customise_checkout_field');
function customise_checkout_field($checkout)
{
	global $wpdb;

	$js = 'jQuery( ".passportDate" ).datepicker({ dateFormat: "dd/mm/yy" });';
	// Check if WC 2.1+
    if ( defined( 'WC_VERSION' ) && WC_VERSION ) {
        wc_enqueue_js( $js );
    } else {
        global $woocommerce;
        $woocommerce->add_inline_js( $js );
    }

	$country_result = $wpdb->get_results ("SELECT * from countries order by name asc");

	echo '<div style="border-top:2px solid #AAAAB6;width:100%; display: inline-block;"></div>
	<div id="customise_checkout_field">
		<h2>' . __('Passport Information') . '</h2>';
?>
	<div class="col2-set" id="passport_details">
		<?php 
		$cart_count = WC()->cart->get_cart_contents_count();
		$product_count = count(WC()->cart->get_cart());
		$item_count = round($cart_count / $product_count);

		for($j=1;$j<=$item_count;$j++){ ?>
		<h5>Person <?php echo $j;?> Passport Details:</h4>
		<div class="col-1">		
			<div class="woocommerce-billing-fields">
				<p class="form-row form-row-first validate-required" id="">
					<label for="passport_number" class="">Passport Number <abbr class="required" title="required">*</abbr></label>
					<input class="input-text " name="passport_number[]" id="" placeholder="" value="" autocomplete="given-name" autofocus="autofocus" type="text" required>
				</p>

				<p class="form-row form-row-last validate-required" id="">
					<label for="passport_holder_name" class="">Passport Holder Name <abbr class="required" title="required">*</abbr></label>
					<input class="input-text " name="passport_name[]" id="" placeholder="" value="" autocomplete="given-name" autofocus="autofocus" type="text" required>
				</p>

				<p class="form-row form-row-first validate-required" id="">
					<label for="passport_issue_date" class="">Passport Issue Date <abbr class="required" title="required">*</abbr></label>
					<input class="input-text passportDate" name="passport_issue_date[]" id="passport_issue_date<?php echo $j;?>" placeholder="dd/mm/yy" value="" autocomplete="given-name" autofocus="autofocus" type="text" required>
				</p>

				<p class="form-row form-row-last validate-required" id="">
					<label for="passport_expiry_date" class="">Passport Expiry Date <abbr class="required" title="required">*</abbr></label>
					<input class="input-text passportDate" name="passport_expiry_date[]" id="passport_expiry_date<?php echo $j;?>" placeholder="dd/mm/yy" value="" autocomplete="given-name" autofocus="autofocus" type="text" required>
				</p>

				<p class="form-row form-row-first validate-required" id="">
					<label for="passport_birth_date" class="">Date of Birth <abbr class="required" title="required">*</abbr></label>
					<input class="input-text passportDate" name="birth_date[]" id="birth_date<?php echo $j;?>" placeholder="dd/mm/yy" value="" autocomplete="given-name" autofocus="autofocus" type="text" required>
				</p>

				<p class="form-row form-row-last validate-required" id="">
					<label for="contact_number" class="">Contact Number <abbr class="required" title="required">*</abbr></label>
					<input class="input-text " name="contact_number[]" id="" placeholder="" value="" autocomplete="given-name" autofocus="autofocus" type="text" required>
				</p>

				<p class="form-row form-row-wide validate-required" id="">
					<label for="passport_country" class="">Country of Passport <abbr class="required" title="required">*</abbr></label>
					<select name="passport_country[]" id="">
						<option value=""> -- Select Country -- </option>
						<?php foreach ( $country_result as $country ) { ?>
						<option value="<?php echo $country->name;?>"><?php echo $country->name;?></option>
						<?php } ?>
					</select>
				</p>

			</div>
		</div>

		<?php if($j < $item_count){ ?>
		<div style="border-top:1px solid #AAAAB6;width:100%; display: inline-block;"></div>
		<?php } ?>

		<?php } ?>

	</div>
	<input type="hidden" name="passport_incre_id" id="passport_incre_id" value="<?php echo $item_count;?>">

<?php echo '</div>'; }

/**
 * Checkout Process
 */
/*add_action('woocommerce_checkout_process', 'customise_checkout_field_process');
function customise_checkout_field_process()
{
	// if the field is set, if not then show an error message.
	if ($_POST['passport_number'] == "") { wc_add_notice(__('Please enter value.') , 'error'); };
}*/

/**
 * Update value of field
 */

add_action('woocommerce_checkout_update_order_meta', 'customise_checkout_field_update_order_meta');
function customise_checkout_field_update_order_meta($order_id)
{
	/*if (!empty($_POST['passport_number'])) {
		update_post_meta($order_id, 'passport_number', sanitize_text_field($_POST['passport_number']));
	}*/
	global $wpdb;

	$get_incre_id = $_POST['passport_incre_id'];
	for($i = 0; $i < $get_incre_id; $i++){
		$wpdb->query("INSERT INTO tour_user_passport_details SET checkout_id = '".$order_id."', passport_id = '".$_POST['passport_number'][$i]."', 	passport_name = '".$_POST['passport_name'][$i]."', passport_issue_date = '".$_POST['passport_issue_date'][$i]."', passport_expiry_date = '".$_POST['passport_expiry_date'][$i]."', passport_country = '".$_POST['passport_country'][$i]."', contact_number = '".$_POST['contact_number'][$i]."', birth_date = '".$_POST['birth_date'][$i]."', type = '1' ");
	}
}


/**
 * Display field value on the order edit page
 */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );
function my_custom_checkout_field_display_admin_order_meta($order){
    echo '<p><h3 style="border-top:1px solid #AAAAB6; padding-top:13px;">Passport Information:</h3> </p>';
    global $wpdb;

    $passport_info = $wpdb->get_results("SELECT * from tour_user_passport_details where checkout_id = '".$order->id."' AND type = 1 ");
    $k = 1;

    foreach ( $passport_info as $key => $passport_det ) {
    	echo '<p><strong>Person '.$k.' Details:</h3> </p>';
    	echo '<div class="address">';
    		echo '<table width="100%">
    			<tr><th style="text-align:left;">Passport Number</th><td>'.$passport_det->passport_id.'</td></tr>
    			<tr><th style="text-align:left;">Passport Name</th><td>'.$passport_det->passport_name.'</td></tr>
    			<tr><th style="text-align:left;">Passport Issue Date</th><td>'.$passport_det->passport_issue_date.'</td></tr>
    			<tr><th style="text-align:left;">Passport Expiry Date</th><td>'.$passport_det->passport_expiry_date.'</td></tr>
    			<tr><th style="text-align:left;">Passport Country</th><td>'.$passport_det->passport_country.'</td></tr>
    			<tr><th style="text-align:left;">Country Number</th><td>'.$passport_det->contact_number.'</td></tr>
    			<tr><th style="text-align:left;">Date of Birth</th><td>'.$passport_det->birth_date.'</td></tr>
    		</table>';
    	echo '</div>';
    	$k++;
    }
}

/**
 * Add the field to order emails
 **/
function passport_info_show_email_order_meta($order) {
    echo '<h3 style="border-top:1px solid #AAAAB6; padding-top:13px;">Passport Information:</h3>';
    global $wpdb;

    $passport_info = $wpdb->get_results("SELECT * from tour_user_passport_details where checkout_id = '".$order->id."' AND type = 1 ");
    $k = 1;

    foreach ( $passport_info as $key => $passport_det ) {
    	echo '<p><strong>Person '.$k.' Details:</h3> </p>';
    		echo '<table width="100%" border="1" cellspacing="0">
    			<tr><th style="text-align:left;">Passport Number</th><td>'.$passport_det->passport_id.'</td></tr>
    			<tr><th style="text-align:left;">Passport Name</th><td>'.$passport_det->passport_name.'</td></tr>
    			<tr><th style="text-align:left;">Passport Issue Date</th><td>'.$passport_det->passport_issue_date.'</td></tr>
    			<tr><th style="text-align:left;">Passport Expiry Date</th><td>'.$passport_det->passport_expiry_date.'</td></tr>
    			<tr><th style="text-align:left;">Passport Country</th><td>'.$passport_det->passport_country.'</td></tr>
    			<tr><th style="text-align:left;">Country Number</th><td>'.$passport_det->contact_number.'</td></tr>
    			<tr><th style="text-align:left;">Date of Birth</th><td>'.$passport_det->birth_date.'</td></tr>
    		</table>';
    	$k++;
    }
}
add_action('woocommerce_email_customer_details', 'passport_info_show_email_order_meta', 30, 3 );
// ============ Passport Information End =====================

/**
 * Remove password strength check.
 */
function iconic_remove_password_strength() {
    wp_dequeue_script( 'wc-password-strength-meter' );
}
add_action( 'wp_print_scripts', 'iconic_remove_password_strength', 10 );


/* ========== Login Redirect ======= */
function wc_custom_user_redirect( $redirect, $user ) {
	// Get the first of all the roles assigned to the user
	$role = $user->roles[0];
	$vendor_registration = $user->vendor_registration;
	$home_url = home_url();
	$vendor_url = home_url('/v_d');
	$myaccount = get_permalink( wc_get_page_id( 'myaccount' ) );
	if( $role == 'customer' &&  ($vendor_registration == "" || $vendor_registration == '0')) {
		//Redirect administrators to the dashboard
		$redirect = $home_url;
	} else if($vendor_registration == 1) {
		//Redirect administrators to the dashboard
		$redirect = $vendor_url;
	} else {
		//Redirect any other role to the previous visited page or, if not available, to the home
		$redirect = $myaccount;
	}
	return $redirect;
}
add_filter( 'woocommerce_login_redirect', 'wc_custom_user_redirect', 10, 2 );
/* ========== Login Redirect ======= */


/* ======== Add Vendor Registration Field ======= */
function wooc_save_extra_register_fields( $customer_id ) {
       if ( isset( $_POST['vendor_registration'] ) ) {
              // WordPress default first name field.
              update_user_meta( $customer_id, 'vendor_registration', sanitize_text_field( $_POST['vendor_registration'] ) );
       }
}
add_action( 'woocommerce_created_customer', 'wooc_save_extra_register_fields' );
/* ======== Add Vendor Registration Field ======= */


/* ======== Remove default vendor role ======= */
add_action('admin_menu', 'remove_built_in_roles');
function remove_built_in_roles() {
    global $wp_roles;
 
    $roles_to_remove = array('vendor', 'pending_vendor');
 
    foreach ($roles_to_remove as $role) {
        if (isset($wp_roles->roles[$role])) {
            $wp_roles->remove_role($role);
        }
    }
}
/* ======== Remove default vendor role ======= */

/* ======== Admin Tour Fields Required Script ========= */
function tour_load_admin_scripts( $hook ) {
	global $post;
	if ( 'product' != $post->post_type ) {
		return;
	}
	// Load the scripts & styles below only if we're creating/updating the post
	if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
			wp_enqueue_script( 'admin_scripts', get_template_directory_uri() . '/assets/js/admin_required_field.js', array( 'jquery' ) );
	}
}
add_action( 'admin_enqueue_scripts', 'tour_load_admin_scripts' );
/* ======== Admin Tour Fields Required Script ========= */


/* =========== Add New Product Push Notification ======== */
add_action( 'woocommerce_process_product_meta', 'woocom_save_general_proddata_custom_field' );
function woocom_save_general_proddata_custom_field( $post_id ) {
	global $wpdb;
	global $post;

	if ( 'product' == $post->post_type ) {
		
		$user_qry_str = "SELECT * FROM userdetails where fcm_token != '' ";
		$user_res = $wpdb->get_results($user_qry_str);

		//print_r($user_res);
		foreach ($user_res as $user){
			//print_r($user);
			$device_token =  $user->fcm_token;

			$path_to_fcm = 'https://fcm.googleapis.com/fcm/send';
			$server_key = "AAAAfB5Q7yQ:APA91bEHOCXtYZ8V6gqvfkg1VCzgDKae5lpTpBPbFxmyiSyWYLOvbkOKStuXZckfemTDDm64FU3TBxUyQwq75Ot7Y9EuAcDhvgGYWWYleR0L2sn50rWa4r2wf1xz-uqAfeNJOOtgia_a";
			$deviceToken = $device_token;
			   

			$message2 = array("status" => 1,"flag" => 1, "status_message" => "Success!", "title" => "New Tour Notification", "notification_status" => "Notification sent", "tour_name" => $_POST['post_title'], "message" => $_POST['post_title']);

			$fields1 = array(
			    'registration_ids' =>array($deviceToken) ,
			    'data' => $message2,                        
			);

			//print_r($fields1);
			$payload = json_encode($fields1);
			//  echo $payload;
			$headers = array(
			    'Authorization:key=' . $server_key,
			    'Content-Type:application/json'
			);

			$curl_session = curl_init();
			curl_setopt($curl_session, CURLOPT_URL, $path_to_fcm);
			curl_setopt($curl_session, CURLOPT_POST, true);
			curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl_session, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl_session, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
			curl_setopt($curl_session, CURLOPT_POSTFIELDS, $payload);

			$result1 = curl_exec($curl_session);
			if (curl_error($curl_session)) {
			//  echo 'error:' . curl_error($curl_session);
			    $status_msg =  'Message not delivered';
			    $status = 0;
			} else{
			  echo $result1;
			    $status_msg =  'Message successfully delivered';
			    $status = 1;
			}
			//     echo  $status_msg ;
			curl_close($curl_session);
		}
	}

}
/* =========== Add New Product Push Notification ======== */