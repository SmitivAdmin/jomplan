<div class="header_top_bar">
	<div class="container">
		<?php /*<div class="row" style="opacity: 0;">
			<?php if ( is_active_sidebar( 'top_bar_left' ) ) : ?>
				<div class="col-sm-3">
					<?php dynamic_sidebar( 'top_bar_left' ); ?>
				</div>
			<?php endif; ?>
			<?php if ( is_active_sidebar( 'top_bar_right' ) ) : ?>
				<div class="col-sm-9 topbar-right">
					<?php dynamic_sidebar( 'top_bar_right' ); ?>
 				</div>
			<?php endif; ?>			
		</div>*/?>

		<?php if(is_user_logged_in()){ 
			$current_user = wp_get_current_user();

			if($current_user->user_firstname != ""){
				$get_first_name = $current_user->user_firstname;
			} else {
				$get_first_name = $current_user->display_name;
			}
		?>
		<div class="welcomeTxt">Welcome <span><?php echo $current_user->display_name; ?></span></div>
		<?php } ?>

		<?php 
			$current_user = wp_get_current_user();
			$user_role = $current_user->roles[0];
			$vendor_registration = get_user_meta($current_user->ID, 'vendor_registration', true);	

			$get_profile_image = get_user_meta($current_user->ID, 'pp_url', true);	
			if($get_profile_image != ""){
				$profile_image_url = home_url('/wp-content/uploads').$get_profile_image;
			} else {
				$profile_image_url = esc_url( get_template_directory_uri() )."/images/user.png";
			}

			if(($user_role == "customer" && $vendor_registration == 1) || $user_role == "dc_vendor" || $user_role == "subscriber"){
				$myaccount_link = "javascript:;";
			} else {
				$myaccount_link = home_url('/my-account');
			}

			if(is_user_logged_in()){ 
		?>	
			<div class="myaccount_link"><img src="<?php echo $profile_image_url; ?>" alt="" title="" width="20" height="20" /><a href="<?php echo $myaccount_link;?>" <?php if(($user_role == "customer" && $vendor_registration != 1) || $user_role == "dc_pending_vendor" || $user_role == "dc_rejected_vendor"){ ?> class="actv" <?php } ?>>Traveler</a> / <a href="<?php echo home_url('/v_d');?>" <?php if(($user_role == "customer" && $vendor_registration == 1) || $user_role == "dc_vendor" || $user_role == "subscriber"){ ?> class="actv" <?php } ?>>Vendor</a>  
			<?php /*if(is_user_logged_in()){ ?>
				<a href="<?php echo wp_logout_url(); ?>" class="logoutLink">Logout</a>
			<?php }*/ ?>
			</div>
		<?php } else { ?>
		
			<div class="myaccount_link"><img src="<?php echo $profile_image_url; ?>" alt="" title="" width="20" height="20" /><a href="<?php echo $myaccount_link;?>" <?php if(is_page('my-account')){ ?> class="actv" <?php } ?>>Traveler</a> / <a href="<?php echo home_url('/v_d');?>" <?php if(is_page('v_d')){ ?> class="actv" <?php } ?>>Vendor</a>
			<?php /*if(is_user_logged_in()){ ?>
				<a href="<?php echo wp_logout_url(); ?>" class="logoutLink">Logout</a>
			<?php }*/ ?>
			</div>
		<?php } ?>
		
	</div>
</div>