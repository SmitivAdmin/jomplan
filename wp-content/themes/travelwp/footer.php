<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package travelWP
 */
?>
</div><!-- #content -->

<div class="wrapper-footer<?php if ( travelwp_get_option( 'show_newsletter' ) == '1' ) {
	echo ' wrapper-footer-newsletter';
} ?>">
	<?php if ( is_active_sidebar( 'footer' ) ) : ?>
		<div class="main-top-footer">
			<div class="container">
				<div class="row">
					<?php dynamic_sidebar( 'footer' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container wrapper-copyright">
		<div class="row">
			<div class="col-sm-6">
				
			</div> <!-- col-sm-3 -->
			<?php if ( is_active_sidebar( 'copyright' ) ) : ?>
				<div class="col-sm-6">
					<?php dynamic_sidebar( 'copyright' ); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>

</div>
<?php
if ( travelwp_get_option( 'show_newsletter' ) == '1' ) {
	$travelwp_theme_options = travelwp_get_data_themeoptions();
	$bg_image               = $travelwp_theme_options['bg_newsletter'] ['url'] ? ' style="background-image: url(' . $travelwp_theme_options['bg_newsletter'] ['url'] . ')"' : '';
	$title                  = travelwp_get_option( 'before_newsletter' ) ? '<div class="title_subtitle">' . travelwp_get_option( 'before_newsletter' ) . '</div>' : '';
	$title .= travelwp_get_option( 'title_newsletter' ) ? '<h3 class="title_primary">' . travelwp_get_option( 'title_newsletter' ) . '</h3>' : '';
	$form = travelwp_get_option( 'shortcode_newsletter' ) ? do_shortcode( travelwp_get_option( 'shortcode_newsletter' ) ) : '';
	echo '<div class="wrapper-subscribe"' . $bg_image . '>
			<div class="subscribe_shadow"></div>
			<div class="form-subscribe parallax-section stick-to-bottom form-subscribe-full-width">
				<div class="shortcode_title text-white shortcode-title-style_1 margin-bottom-3x">
				' . $title . '
				<span class="line_after_title"></span>
				</div>
				<div class="form-subscribe-form-wrap">
					<aside class="mailchimp-container">' . $form . ' </aside>
				</div>
			</div>
		</div>
 	';
}

$usercount = get_users( 'role=customer' );
$usercount_result = count($usercount); 

$vendorcount = get_users( 'role=dc_vendor' );
$vendorcount_result = count($vendorcount); 

$total_deatinations = get_terms('pa_destination', array('hide_empty' => false, 'fields' => 'count') );

$total_tours = count( get_posts( array('post_type' => 'product', 'product_type' => 'tour_phys', 'post_status' => 'publish', 'fields' => 'ids', 'posts_per_page' => '-1') ) );

$tour_types_args = array(
    'taxonomy'   => "tour_phys",
    'post_type'     => 'product',
);
$tour_types_categories = get_terms($tour_types_args);
$total_tour_types = count($tour_types_categories);

?>
</div>

<script type="text/javascript">
	jQuery(".traveller_count .stats_counter_number").html('<?php echo $usercount_result;?>');

	jQuery(".agencies_count .stats_counter_number").html('<?php echo $vendorcount_result;?>');

	jQuery(".destinations_count .stats_counter_number").html('<?php echo $total_deatinations;?>');

	jQuery(".tours_count .stats_counter_number").html('<?php echo $total_tours;?>');

	jQuery(".tour_types_count .stats_counter_number").html('<?php echo $total_tour_types;?>');

	jQuery(document).ready(function(){
		jQuery("#search-box").keyup(function(){
			jQuery.ajax({
				type: "POST",
				url: "<?php echo get_site_url();?>/autosearch_ajax.php",
				data:'keyword='+jQuery(this).val(),
				success: function(data){
					jQuery("#suggesstion-box").show();
					jQuery("#suggesstion-box").html(data);
					jQuery("#search-box").css("background","#FFF");
				}
			});
		});
	});

	function selectCountry(val) {
		jQuery("#search-box").val(val);
		jQuery("#suggesstion-box").hide();
	}
</script>

<?php wp_footer();
?>
</body>
</html>
