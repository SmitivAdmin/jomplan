<?php
/**
 * Order details frontend
 *
 * @author : Physcode
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$order = wc_get_order( $order_id );

$show_purchase_note    = $order->has_status( apply_filters( 'woocommerce_purchase_note_order_statuses', array( 'completed', 'processing' ) ) );
$show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id();

$order_items = $order->get_items();
if($order->get_status()){
	foreach ( $order_items as $item_id => $item ) {
		if( have_rows('product_download_details', $item['product_id']) ){
			$count = 0;
			while( have_rows('product_download_details', $item['product_id']) ){ 
				the_row();
				$count++;
			}
		}
	}

	if($count > 0){
	echo '<section class="woocommerce-customer-details">';
		echo '<h2 class="woocommerce-column__title">Product Informations:</h2>';
		echo '<address>';
		foreach ( $order_items as $item_id => $item ) {
			/*$product = $item->get_product();
			echo $item['product_id'];*/

			if( have_rows('product_download_details', $item['product_id']) ){
				while( have_rows('product_download_details', $item['product_id']) ){ 
					the_row(); 
		    		$product_description = get_sub_field('product_description');
		    		$product_download_pdf_file = get_sub_field('product_download_pdf_file');
		    		$ext = pathinfo($product_download_pdf_file['url'], PATHINFO_EXTENSION);
		    		echo '<span class="productInfoCnt">';
			    		echo '<span class="productTitle">'.$item['name'].'</span>';
			    		echo '<span class="productDesc">'.$product_description.'</span>';
			    		if($product_download_pdf_file['url'] != ""){
			    			echo '<span class="productFile"><a href="'.$product_download_pdf_file['url'].'" target="_blank">Click to View and Download PDF File.</a></span>';
			    		}
		    		echo '</span>';
		    	}
		    }
		}
		echo '</address>';
	echo '</section>';
  }
}
?>
<h2><?php _e( 'Order Details', 'travel-booking' ); ?></h2>
<table class="shop_table order_details">
	<thead>
	<tr>
		<th class="product-name"><?php _e( 'Tour', 'travel-booking' ); ?></th>
		<th class="product-total"><?php _e( 'Total', 'travel-booking' ); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php
	foreach ( $order->get_items() as $item_id => $item ) {
		$product = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item );

		wc_get_template(
			'order/order-details-item.php', array(
			'order'              => $order,
			'item_id'            => $item_id,
			'item'               => $item,
			'show_purchase_note' => $show_purchase_note,
			'purchase_note'      => $product ? get_post_meta( $product->get_id(), '_purchase_note', true ) : '',
			'product'            => $product,
		)
		);
	}
	?>
	<?php do_action( 'woocommerce_order_items_table', $order ); ?>
	</tbody>
	<tfoot>
	<?php
	foreach ( $order->get_order_item_totals() as $key => $total ) {

		if($total['label'] != "Shipping:"){
		?>
		<tr>
			<th scope="row"><?php echo $total['label']; ?></th>
			<td><?php echo $total['value']; ?></td>
		</tr>
		<?php

		}
	}
	?>
	</tfoot>
</table>

<?php do_action( 'woocommerce_order_details_after_order_table', $order ); ?>

<?php if ( $show_customer_details ) : ?>
	<?php wc_get_template( 'order/order-details-customer.php', array( 'order' => $order ) ); ?>
<?php endif; ?>
