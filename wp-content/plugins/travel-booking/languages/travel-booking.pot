#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-05-29 08:46+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco - https://localise.biz/"

#: D:/svn/wp/theme-plugins/travel-booking/travel-booking.php:47
msgid "Media Categories"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/travel-booking.php:48
msgid "Media Category"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/travel-booking.php:128
msgid "Tour Types"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/travel-booking.php:129
#: D:/svn/wp/theme-plugins/travel-booking/travel-booking.php:138
#: D:/svn/wp/theme-plugins/travel-booking/inc/widgets.php:37
msgid "Tour Type"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/travel-booking.php:130
msgid "Search Tour Types"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/travel-booking.php:131
msgid "All Tour Types"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/travel-booking.php:132
msgid "Parent Tour Type"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/travel-booking.php:133
msgid "Parent Tour Type:"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/travel-booking.php:134
msgid "Edit Tour Type"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/travel-booking.php:135
msgid "Update Tour Type"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/travel-booking.php:136
msgid "Add New Tour Type"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/travel-booking.php:137
msgid "New Tour Type Name"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/travel-booking.php:226
msgid "Please active plugin Woocomerce before active plugin Travel Booking"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/metabox-booking.php:26
msgid "Tour Code"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/metabox-booking.php:30
msgid "Number ticket limits"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/metabox-booking.php:37
msgid "Default 0 is get \"Price child\" by percent value on setting Tour"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/metabox-booking.php:41
msgid "Duration"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/metabox-booking.php:42
msgid "5 Days 4 Nights"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/metabox-booking.php:45
msgid "Start Date"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/metabox-booking.php:49
msgid "End Date"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/metabox-booking.php:53
msgid "Days of Tour and price for each day"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/metabox-booking.php:57
msgid ""
"Default \"Price Adults\" 0 will get price \"Regular\" or \"Sale\" price (if "
"have) for it"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/metabox-booking.php:58
msgid "Default \"Price Children\" 0 will get price \"Price of child\" for it"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/metabox-booking.php:60
msgid "Set Default 0"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/metabox-booking.php:85
msgid "Price Adults"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/metabox-booking.php:86
msgid "Price Children"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/metabox-booking.php:103
#: D:/svn/wp/theme-plugins/travel-booking/inc/tb-get-template.php:108
msgid "Itinerary"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/metabox-booking.php:104
#: D:/svn/wp/theme-plugins/travel-booking/inc/tb-get-template.php:116
msgid "Location"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/metabox-booking.php:114
msgid "Address"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/metabox-booking.php:118
msgid "Latitude"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/metabox-booking.php:122
msgid "Longitude"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/product-type-tour.php:20
#: D:/svn/wp/theme-plugins/travel-booking/templates/cart.php:25
#: D:/svn/wp/theme-plugins/travel-booking/templates/cart.php:70
#: D:/svn/wp/theme-plugins/travel-booking/templates/order-details.php:21
#: D:/svn/wp/theme-plugins/travel-booking/templates/review-order.php:15
#: D:/svn/wp/theme-plugins/travel-booking/templates/emails/email-order-details.php:34
msgid "Tour"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/show-booking-date-order.php:11
msgid "Booking Date"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/show-booking-date-order.php:13
msgid "Aduls"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/show-booking-date-order.php:16
#: D:/svn/wp/theme-plugins/travel-booking/templates/cart.php:113
#: D:/svn/wp/theme-plugins/travel-booking/templates/order-details-item.php:33
#: D:/svn/wp/theme-plugins/travel-booking/templates/review-order.php:42
#: D:/svn/wp/theme-plugins/travel-booking/templates/emails/email-order-items.php:48
#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/booking.php:92
msgid "Children"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/sidebar.php:5
msgid "Sidebar Tour"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tab-booking.php:5
msgid "Tour Booking"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tab-booking.php:6
msgid "Tour Data"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/taxonomy_tour.php:10
msgid "Tour Categories"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/taxonomy_tour.php:11
#: D:/svn/wp/theme-plugins/travel-booking/inc/taxonomy_tour.php:20
msgid "Tour Category"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/taxonomy_tour.php:12
msgid "Search Tour Categories"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/taxonomy_tour.php:13
msgid "All Tour Categories"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/taxonomy_tour.php:14
msgid "Parent Tour Category"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/taxonomy_tour.php:15
msgid "Parent Tour Category:"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/taxonomy_tour.php:16
msgid "Edit Tour Category"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/taxonomy_tour.php:17
msgid "Update Tour Category"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/taxonomy_tour.php:18
msgid "Add New Tour Category"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/taxonomy_tour.php:19
msgid "New Tour Category Name"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tb-get-template.php:101
msgid "Description"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tb-get-template.php:125
#, php-format
msgid "Reviews (%d)"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:30
msgid "Tours"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:48
msgid "Settings search tours"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:54
msgid "Search by attribute"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:55
msgid "Search tour by attribute set on Woocomerce"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:70
msgid "Settings permalink tours"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:76
msgid "Tour category base"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:90
msgid "Tour Pages"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:96
msgid "Tours Page"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:103
msgid ""
"This sets the base page of your tour - this is where your tour archive will "
"be."
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:106
msgid "Tour Expired"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:107
msgid "Show (Yes) or hide (No) tours Expire on list Tours"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:116
msgid "Redirect to page after Booking tour"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:125
msgid "Google API Key"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:126
msgid "Use show google map in tab Location of single tour"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:127
msgid ""
"How to get API Key https://developers.google."
"com/maps/documentation/javascript/get-api-key"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:134
msgid "Separate Ticket for Adult, Children"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:144
msgid "Price percent child/adult (%)"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:146
msgid ""
"If you want set price children for each Tour, You can set set value Price "
"Child on Tour when \"Show number adults and children\" enable"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:175
msgid "Choose attributes&hellip;"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:176
msgid "Attributes"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:185
msgid "Select all"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:186
msgid "Select none"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:194
msgid "General"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:195
msgid "Search Tours"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/tour_tab_settings.php:196
msgid "Permalink Tours"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/widgets.php:18
msgid "Tour Search"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/widgets.php:28
#: D:/svn/wp/theme-plugins/travel-booking/inc/widgets.php:33
msgid "Search Tour"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/widgets.php:29
msgid "Find your dream tour today!"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/inc/widgets.php:75
msgid "Find Tours"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/archive-attribute.php:42
#: D:/svn/wp/theme-plugins/travel-booking/templates/archive-tour.php:42
msgid "Default sorting"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/archive-attribute.php:43
#: D:/svn/wp/theme-plugins/travel-booking/templates/archive-tour.php:43
msgid "Sort by popularity"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/archive-attribute.php:44
#: D:/svn/wp/theme-plugins/travel-booking/templates/archive-tour.php:44
msgid "Sort by average rating"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/archive-attribute.php:45
#: D:/svn/wp/theme-plugins/travel-booking/templates/archive-tour.php:45
msgid "Sort by newness"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/archive-attribute.php:46
#: D:/svn/wp/theme-plugins/travel-booking/templates/archive-tour.php:46
msgid "Sort by price: low to high"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/archive-attribute.php:47
#: D:/svn/wp/theme-plugins/travel-booking/templates/archive-tour.php:47
msgid "Sort by price: high to low"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/archive-attribute.php:77
#: D:/svn/wp/theme-plugins/travel-booking/templates/archive-tour.php:77
msgid "No tours were found matching your selection."
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/cart.php:26
#: D:/svn/wp/theme-plugins/travel-booking/templates/cart.php:93
#: D:/svn/wp/theme-plugins/travel-booking/templates/emails/email-order-details.php:35
#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/price.php:27
msgid "Price"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/cart.php:27
#: D:/svn/wp/theme-plugins/travel-booking/templates/cart.php:105
#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/booking.php:83
#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/booking.php:120
msgid "Number ticket"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/cart.php:28
#: D:/svn/wp/theme-plugins/travel-booking/templates/cart.php:131
#: D:/svn/wp/theme-plugins/travel-booking/templates/order-details.php:22
#: D:/svn/wp/theme-plugins/travel-booking/templates/review-order.php:16
#: D:/svn/wp/theme-plugins/travel-booking/templates/review-order.php:108
#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/booking.php:106
#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/booking.php:116
msgid "Total"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/cart.php:50
msgid "Remove this item"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/cart.php:78
#: D:/svn/wp/theme-plugins/travel-booking/templates/order-details-item.php:28
#: D:/svn/wp/theme-plugins/travel-booking/templates/review-order.php:37
#: D:/svn/wp/theme-plugins/travel-booking/templates/emails/email-order-items.php:43
msgid "Booking Date:"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/cart.php:88
msgid "Available on backorder"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/cart.php:111
#: D:/svn/wp/theme-plugins/travel-booking/templates/order-details-item.php:30
#: D:/svn/wp/theme-plugins/travel-booking/templates/review-order.php:39
#: D:/svn/wp/theme-plugins/travel-booking/templates/emails/email-order-items.php:45
#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/booking.php:79
msgid "Adults"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/cart.php:149
msgid "Coupon:"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/cart.php:154
msgid "Coupon code"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/cart.php:156
msgid "Apply Coupon"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/cart.php:163
msgid "Update Cart"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/order-details.php:17
msgid "Order Details"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/review-order.php:61
msgid "Subtotal"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/emails/email-order-details.php:26
#, php-format
msgid "Order #%s"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/global/breadcrumb.php:23
msgid "Home"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/loop/result-count.php:39
#, php-format
msgid "Showing the single result"
msgid_plural "Showing all %d results"
msgstr[0] ""
msgstr[1] ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/loop/result-count.php:41
#, php-format
msgctxt "%1$d = first, %2$d = last, %3$d = total"
msgid "Showing the single result"
msgid_plural "Showing %1$d&ndash;%2$d of %3$d results"
msgstr[0] ""
msgstr[1] ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/booking.php:41
msgid "Book the tour"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/booking.php:47
msgid "First name"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/booking.php:53
msgid "Last name"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/booking.php:59
msgid "Email"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/booking.php:65
msgid "Phone"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/booking.php:70
msgid "Date Book"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/booking.php:83
#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/booking.php:120
msgid "Number ticket of Adults"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/booking.php:96
msgid "Number ticket of Children"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/booking.php:141
msgid "Booking now"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/code.php:20
msgid "Code:"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/gallery.php:58
msgid "Placeholder"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/related.php:54
msgid "Related Tours"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/review-rating.php:32
#, php-format
msgid "Rated %s out of 5"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/review-rating.php:34
#, php-format
msgid "out of %s5%s"
msgstr ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/review-rating.php:35
#, php-format
msgid "based on %s customer rating"
msgid_plural "based on %s customer ratings"
msgstr[0] ""
msgstr[1] ""

#: D:/svn/wp/theme-plugins/travel-booking/templates/single-tour/review-rating.php:39
#, php-format
msgid "%s review"
msgid_plural "%s reviews"
msgstr[0] ""
msgstr[1] ""

#. Name of the plugin
msgid "Travel booking"
msgstr ""

#. Description of the plugin
msgid "Option for Tour"
msgstr ""

#. Author of the plugin
msgid "Physcode"
msgstr ""

#. Author URI of the plugin
msgid "http://physcode.com/"
msgstr ""
