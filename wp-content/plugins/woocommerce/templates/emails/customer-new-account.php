<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>
	
	<?php 
		$user = get_user_by( 'login', $user_login ); // Get the user object (from the user login)
		$user_id = $user->ID; // Get the user ID
		/*$user_data = get_userdata( $user_id );
		$user_role = $user_data->roles[0];
		$user_roles = implode(', ', $user_data->roles);*/
		$user_data = get_userdata( $user_id );
		$vendor_registration = $user_data->vendor_registration;
	?>

	<p><?php printf( __( 'Thanks for creating an account on %1$s. Your username is %2$s', 'woocommerce' ), esc_html( $blogname ), '<strong>' . esc_html( $user_login ) . '</strong>' ); ?></p>

<?php if ( 'yes' === get_option( 'woocommerce_registration_generate_password' ) && $password_generated ) : ?>

	<p><?php printf( __( 'Your password has been automatically generated: %s', 'woocommerce' ), '<strong>' . esc_html( $user_pass ) . '</strong>' ); ?></p>

<?php endif; ?>
	
	<?php if($vendor_registration == 1){ ?>
	<p><?php printf( __( 'You can access your vendor dashboard here: %s.', 'woocommerce' ), make_clickable( esc_url( home_url('/v_d') ) ) ); ?></p>
	<?php } else { ?>
	<p><?php printf( __( 'You can access your account area to view your orders and change your password here: %s.', 'woocommerce' ), make_clickable( esc_url( wc_get_page_permalink( 'myaccount' ) ) ) ); ?></p>
	<?php } ?>

<?php do_action( 'woocommerce_email_footer', $email );
