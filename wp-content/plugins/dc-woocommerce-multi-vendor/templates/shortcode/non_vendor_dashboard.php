<?php
/**
 * The template for displaying vendor dashboard for non-vendors
 *
 * Override this template by copying it to yourtheme/dc-product-vendor/shortcode/non_vendor_dashboard.php
 *
 * @author 		WC Marketplace
 * @package 	WCMm/Templates
 * @version   2.2.0
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly
global $woocommerce, $WCMp, $wpdb;
$user = wp_get_current_user();

$memplan_query = $wpdb->get_results("SELECT * from wpny_posts where post_type = 'pms-subscription' AND post_status = 'active' ");
$memplan_count = count($memplan_query);

echo '<div class="woocommerce">';
    
if($memplan_count > 0){

    echo do_shortcode('[pms-subscriptions]');
    
    $member_info = $wpdb->get_results("SELECT * from wpny_pms_member_subscriptions where user_id = '".$user->id."' AND status = 'active' ");
    $member_count = count($member_info);

    if(($user->roles[0] == "subscriber" || $user->roles[0] == "dc_vendor") && $member_count > 0){

        if ($user && !in_array('dc_pending_vendor', $user->roles) && !in_array('administrator', $user->roles)) {
            add_filter('wcmp_vendor_registration_submit', function ($text) {
                return 'Apply to become a vendor';
            });
            //echo '<div class="woocommerce">';
            echo do_shortcode('[vendor_registration]');
            //echo '</div>';
        }

        if ($user && in_array('administrator', $user->roles)) {
            ?>
            <div class="vendor_apply">
                <p>
                    <?php _e('You have logged in as Administrator. Please log out and then view this page.', 'dc-woocommerce-multi-vendor'); ?>
                </p>
            </div>
            <?php
        }
        if ($user && in_array('dc_pending_vendor', $user->roles)) {
            ?>
            <div class="vendor_apply">
                <p>
                    <?php _e('Congratulations! You have successfully applied as a Vendor. Please wait for further notifications from the admin.', 'dc-woocommerce-multi-vendor'); ?>
                </p>
            </div>
            <?php
        }

    }

} else {
    if ($user && !in_array('dc_pending_vendor', $user->roles) && !in_array('administrator', $user->roles)) {
        add_filter('wcmp_vendor_registration_submit', function ($text) {
            return 'Apply to become a vendor';
        });
        //echo '<div class="woocommerce">';
        echo do_shortcode('[vendor_registration]');
        //echo '</div>';
    }

    if ($user && in_array('administrator', $user->roles)) {
        ?>
        <div class="vendor_apply">
            <p>
                <?php _e('You have logged in as Administrator. Please log out and then view this page.', 'dc-woocommerce-multi-vendor'); ?>
            </p>
        </div>
        <?php
    }
    if ($user && in_array('dc_pending_vendor', $user->roles)) {
        ?>
        <div class="vendor_apply">
            <p>
                <?php _e('Congratulations! You have successfully applied as a Vendor. Please wait for further notifications from the admin.', 'dc-woocommerce-multi-vendor'); ?>
            </p>
        </div>
        <?php
    }
}

if(($user->roles[0] == "dc_pending_vendor")){
    echo '<a href="'.wp_logout_url().'" class="logoutLinkBtn">Logout</a>';
}
echo '</div>';