<?php
include('../include/config.php');

require_once 'billplz.php';
require_once 'configuration.php';

$checkout_id = $_GET['checkout_id'];
$email_id = $_GET['email_id'];
$tour_id = $_GET['tour_id'];

class verifytrans {

    var $billplz;
    var $data;
    var $moreData;

    function __construct() {
        /*
         * Get Data. Die if input is tempered or X Signature not enabled
         */
        global $x_signature;
        $this->data = Billplz::getRedirectData($x_signature);
        $this->billplz = new Billplz;
    }

    function checkStatus() {
        global $api_key;
        $bill_id = $this->data['id'];
        $this->moreData = $this->billplz->check_bill($api_key, $bill_id);
        return $this;
    }

    /*
     * Dalam variable $this->data ada maklumat berikut:
     * 1. id //bill_id
     * 2. paid_at
     * 3. paid
     * 4. x_signature
     */

    function process($checkout_id,$email_id,$tour_id,$con) {
        global $successpath;
        if ($this->data['paid']) {
            //////////////////////////////////////////////////
            // Include tracking code here
            //////////////////////////////////////////////////
            /*if (!empty($_GET['successpath'])) {
                if (!headers_sent()) {
                    header('Location: ' . base64_decode($_GET['successpath']));
                } else {
                    echo "If you are not redirected, please click <a href=" . '"' . base64_decode($_GET['successpath']) . '"' . " target='_self'>Here</a><br />"
                    . "<script>location.href = '" . base64_decode($_GET['successpath']) . "'</script>";
                }
            } else {
                if (!headers_sent()) {
                    header('Location: ' . $successpath);
                } else {
                    echo "If you are not redirected, please click <a href=" . '"' . $successpath . '"' . " target='_self'>Here</a><br />"
                    . "<script>location.href = '" . $successpath . "'</script>";
                }
            }*/



            $payment_status = 1;
            $payment_msg = "Payment Successful, We will send confirmation soon.";

        } else {
            /*if (!headers_sent()) {
                header('Location: ' . $this->moreData['url']);
            } else {
                echo "If you are not redirected, please click <a href=" . '"' . $this->data['url'] . '"' . " target='_self'>Here</a><br />"
                . "<script>location.href = '" . $this->moreData['url'] . "'</script>";
            }*/
            
            $payment_status = 0;
            $payment_msg = "Payment Transaction Error.";
        }

        /*$sql_str = "update tour_checkout set tour_status = '".$payment_status."', bill_id = '".$this->moreData['id']."', bill_url = '".$this->moreData['url']."' where id = '".$checkout_id."' ";

        $query = mysqli_query($con, $sql_str);*/

        if($payment_status == 1){
            $current_date = date('Y-m-d H:i:s');

            $update_paid_status = mysqli_query($con, "update wpny_posts set post_modified = '".$current_date."', post_modified_gmt = '".$current_date."', post_status = 'wc-completed' WHERE ID = '".$checkout_id."' ");

            if($update_paid_status){
                $update_meta_query_1 = mysqli_query($con, "update wpny_postmeta meta_value = '".strtotime($current_date)."' WHERE post_id = '".$checkout_id."' AND meta_key = '_date_completed'"); 
                $update_meta_query_2 = mysqli_query($con, "update wpny_postmeta meta_value = '".$current_date."' WHERE post_id = '".$checkout_id."' AND meta_key = '_completed_date'");

                $select_post_meta_query = mysqli_query($con, "select meta_id,meta_value from wpny_postmeta where meta_key = '_wcpdf_invoice_number' order by meta_id desc limit 0,1 ");
                $select_post_meta_res = mysqli_fetch_array($select_post_meta_query);

                $invoice_number = $select_post_meta_res['meta_value'] + 1;

                $isert_post_meta_query = mysqli_query($con, "insert into wpny_postmeta (post_id,meta_key,meta_value) VALUES
                    ('".$checkout_id."', '_payment_method', 'billplz'),
                    ('".$checkout_id."', '_payment_method_title', 'Online Payment'),
                    ('".$checkout_id."', '_download_permissions_granted', 'yes'),
                    ('".$checkout_id."', '_recorded_sales', 'yes'),
                    ('".$checkout_id."', '_recorded_coupon_usage_counts', 'yes'),
                    ('".$checkout_id."', '_commissions_processed', 'yes'),
                    ('".$checkout_id."', '_wcpdf_invoice_date', '".strtotime($current_date)."'),
                    ('".$checkout_id."', '_wcpdf_invoice_date_formatted', '".$current_date."'),
                    ('".$checkout_id."', '_wcpdf_invoice_number', '".$invoice_number."')
                ");

            } else {
                $json = array("status" => 0, "msg" => "Error Occured");
                header('Content-type: application/json');
                echo json_encode($json);
            }
        }

        echo '<center style="margin-top:30px;">'.$payment_msg.' Please press the back button.</center>';

    }

}

$verifytrans = new verifytrans();
$verifytrans->checkStatus()->process($checkout_id,$email_id,$tour_id,$con);
//$verifytrans->process();
