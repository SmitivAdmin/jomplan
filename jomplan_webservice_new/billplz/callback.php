<?php
include('include/config.php');

require_once 'billplz.php';
require_once 'configuration.php';

$checkout_id = $_GET['checkout_id'];
$profile_id = $_GET['profile_id'];
$tour_id = $_GET['tour_id'];

/*
 * Get Data. Die if input is tempered or X Signature not enabled
 */
$data = Billplz::getCallbackData($x_signature);
$tranID = $data['id'];

$billplz = new Billplz;
$moreData = $billplz->check_bill($api_key, $tranID);

/*
 * Dalam variable $moreData ada maklumat berikut (array):
 * 1. reference_1
 * 2. reference_1_label
 * 3. reference_2
 * 4. reference_2_label
 * 5. amount
 * 6. description
 * 7. id // bill_id
 * 8. name
 * 9. email
 * 10. paid
 * 11. collection_id
 * 12. due_at
 * 13. mobile
 * 14. url
 * 15. callback_url
 * 16. redirect_url
 * 
 * Contoh untuk akses data email: $moreData['email'];
 * 
 * Dalam variable $data ada maklumat berikut (array):
 * 1. x_signature
 * 2. id // bill_id
 * 3. paid
 * 4. paid_at
 * 5. amount
 * 6. collection_id
 * 7. due_at
 * 8. email
 * 9. mobile
 * 10. name
 * 11. paid_at
 * 12. state
 * 13. url
 * 
 * Contoh untuk ases data bill_id: $data['id']
 * 
 */

/*
 * Jika bayaran telah dibuat
 */
if ($data['paid']) {

	$payment_status = 1;
    $payment_msg = "Payment Successful, We will send confirmation soon.";

}
/*
 * Jika bayaran tidak dibuat
 */ else {

	$payment_status = 0;
    $payment_msg = "Payment Transaction Error.";

}

$json = array("status" => 1, "payment_status" => 1, "payment_msg" =>"Paid", "bill_id" =>$moreData['id'], "bill_url" =>$moreData['url']);
header('Content-type: application/json');
echo json_encode($json);


$query = mysqli_query($con, "update tour_checkout set tour_status = '".$payment_status."', bill_id = '".$this->moreData['id']."', bill_url = '".$this->moreData['url']."' where id = '".$checkout_id."' ");

$get_checkout_query = mysqli_query($con, "select * tour_checkout where id = '".$checkout_id."' ");
$get_checkout_res = mysqli_fetch_array($get_checkout_query);

if ($query) {

    if($tour_status == 1){

        $get_user_query = mysqli_query($con, "select * from userdetails WHERE profile_id = '".$profile_id."' ");
        $get_user_res = mysqli_fetch_array($get_user_query);

        //$get_tour_query = mysqli_query($con, "select * from tour_details WHERE id = '".$tour_id."' ");
         $tour_sql_str = "SELECT wpny_posts.* FROM wpny_term_relationships LEFT JOIN wpny_posts ON wpny_term_relationships.object_id = wpny_posts.ID LEFT JOIN wpny_term_taxonomy ON wpny_term_taxonomy.term_taxonomy_id = wpny_term_relationships.term_taxonomy_id LEFT JOIN wpny_terms ON wpny_terms.term_id = wpny_term_relationships.term_taxonomy_id WHERE wpny_posts.post_type = 'product' AND wpny_term_taxonomy.taxonomy = 'tour_phys' AND wpny_posts.ID = '".$tour_id."' group by wpny_posts.ID";
        $get_tour_query = mysqli_query($con, $tour_sql_str);
        $get_tour_res = mysqli_fetch_array($get_tour_query);

        $tour_name = $get_tour_res['post_title'];   
        $reference_no = $get_checkout_res['reference_no'];
        if($get_checkout_res['flights'] == 1){
            $flights_trip = "One Way";
        } else {
            $flights_trip = "Round Trip";
        }
        $depature_date = $get_checkout_res['tour_date'];
        $return_date = $get_checkout_res['return_date'];
        $adults = $get_checkout_res['adults'];
        $teens = $get_checkout_res['teens'];
        $childrens = $get_checkout_res['childrens'];
        $infants = $get_checkout_res['infants'];
        $tour_price_per_person = $get_checkout_res['tour_price_per_person'];
        $tour_total_price = $get_checkout_res['tour_total_price'];
        $created_date = $get_checkout_res['created_date'];

        $from = "admin@jomplan.com";
        $to = $get_user_res['email'];
        $user_name = $get_user_res['first_name'];

        $subject = "Jomplan - Your Tour(".$tour_name.") Booking Confirmed.";
        $headers = "From: Jomplan - Smarter Way To Travel<" . $from . "> \r\n";
        $headers .= "Return-Path: " . $from . " \r\n";
        $headers .= "Reply-To: " . $from . " \r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        $headers .= "X-Priority: 3\r\n";
        $headers .= "X-Mailer: PHP" . phpversion() . "\r\n";

        $message = "<html>"
                . "<head></head>"
                . "<body>"
                . "<h3>Hello " . $user_name . ",</h3><br>Your tour(<b>".$tour_name."</b>) booking confirmed, your booking reference number <b>".$reference_no."</b>. Please find the following details:<br><br>"
                . "<table cellpadding='5' cellspacing='0' border='1' width='80%'>
                    <tr><th style='text-align:left'>Tour Name</th><td>".$tour_name."</td></tr>
                    <tr><th style='text-align:left'>Flights</th><td>".$flights_trip."</td></tr>
                    <tr><th style='text-align:left'>Depature Date</th><td>".date('M d, Y', strtotime($depature_date))."</td></tr>
                    <tr><th style='text-align:left'>Return Date</th><td>".date('M d, Y', strtotime($return_date))."</td></tr>
                    <tr><th style='text-align:left'>Adults</th><td>".$adults."</td></tr>
                    <tr><th style='text-align:left'>Teens</th><td>".$teens."</td></tr>
                    <tr><th style='text-align:left'>Childrens</th><td>".$childrens."</td></tr>
                    <tr><th style='text-align:left'>Infants</th><td>".$infants."</td></tr>
                    <tr><th style='text-align:left'>Tour Price/Person  </th><td>$".$tour_price_per_person."</td></tr>
                    <tr><th style='text-align:left'>Total Price </th><td><b>$".$tour_total_price."</b></td></tr>
                    <tr><th style='text-align:left'>Tour Booked Date </th><td><b>".date('M d, Y', strtotime($created_date))."</b></td></tr>
                    </table>"
                . "<br><br><p>Regards,<br>Jomplan - Smarter Way To Travel</p>"
                . "</body>"
                . "</html>";
     
        mail($to, $subject, $message, $headers);
    }


    /*$json = array("status" => 1, "payment_status" => $payment_status, "payment_msg" =>$payment_msg, "bill_id" =>$this->moreData['id'], "bill_url" =>$this->moreData['url']);
    header('Content-type: application/json');
    echo json_encode($json);*/

    echo '<center style="margin-top:30px;">'.$payment_msg.' Please press the back button.</center>';
} else {
    $json = array("status" => 0, "msg" => "Error Occured");
    header('Content-type: application/json');
    echo json_encode($json);
}
