<?php
/**
 * Instruction: 
 *  
 * 1. Replace the APIKEY with your API Key.
 * 2. OPTIONAL: Replace the COLLECTION with your Collection ID.
 * 3. Replace the X_SIGNATURE with your X Signature Key
 * 4. Replace the http://www.google.com/ with your FULL PATH TO YOUR WEBSITE. It must be end with trailing slash "/".
 * 5. Replace the http://www.google.com/success.html with your FULL PATH TO YOUR SUCCESS PAGE. *The URL can be overridden later
 * 6. OPTIONAL: Set $amount value.
 * 7. OPTIONAL: Set $fallbackurl if the user are failed to be redirected to the Billplz Payment Page.
 * 
 */

// Live
$api_key = '96ece5bc-d58f-4f25-a4e3-acdc59629e2a';
$collection_id = 'kqvb25f_';
$x_signature = 'S-8JGSFwEn4b0lA6IjNvzMYQ';

// Sandbox
/*$api_key = 'c20a8984-e671-478a-97ff-e5679178cff6';
$collection_id = 'o4yzxabx';
$x_signature = 'S-ahVubPP8ToGgzchB468vjQ';*/

$websiteurl = 'http://www.jomplan.com/jomplan_webservice_new/billplz/';
$successpath = 'http://www.jomplan.com/';
$amount = ''; //Example (RM13.50): $amount = '13.50';
$fallbackurl = 'http://www.jomplan.com/'; //Example: $fallbackurl = 'http://www.google.com/pay.php';
