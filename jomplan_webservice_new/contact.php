<?php
include('include/config.php');
if ($con) {


    $query = mysqli_query($con, "select * from address");
    if (mysqli_num_rows($query) > 0) {
        $list = array();
        while($res = mysqli_fetch_array($query)){
           $id = $res['id'];
            $address = stripcslashes($res['address']);
           $contact_office_no = $res['contact_office_no'];
           $contact_ph=$res['contact_ph'];
           $contact_email=$res['contact_email'];

            $list[] = array("id" => $id,"address" =>$address,"contact_office_no"=>$contact_office_no,"contact_ph" =>$contact_ph,"contact_email" =>$contact_email);
        }

        $json = array("Status" => 1, "Message" => "Contact Address", "list" => $list);
        header('Content-type: application/json');
        echo json_encode($json);

    } else {
        $json = array("Status" => 0, "Message" => "No tours found.");
        header('Content-type: application/json');
        echo json_encode($json);
    }
    
} else {

    $json = array("Status" => 0, "Message" => "Network Error");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>