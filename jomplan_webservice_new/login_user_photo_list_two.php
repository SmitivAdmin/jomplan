<?php
include('include/config.php');
$current_url = "http://" . $_SERVER['HTTP_HOST'] . '/jomplan_webservice_new/';

if ($con) {
    if (($_POST["email"] != "")) {

        $email = $_POST['email'];

        //$login_user_query = mysqli_query($con, "select * from userdetails where profile_id = '".$profile_id."'");
        $login_user_query = mysqli_query($con, "select u.*, um1.meta_value as user_first_name, um2.meta_value as user_last_name from wpny_users u left join wpny_usermeta um1 on (u.ID = um1.user_id AND um1.meta_key = 'first_name') left join wpny_usermeta um2 on (u.ID = um2.user_id AND um2.meta_key = 'last_name') where user_email='".$_POST["email"]."' group by u.ID");
        $login_user_res = mysqli_fetch_array($login_user_query);

        $first_name = $login_user_res['user_first_name'];
        $last_name = $login_user_res['user_last_name'];
        $profile_photo = $login_user_res['imgurl'];
        $user_cover_photo = $login_user_res['user_cover_photo'];

        $flwing_user_qry = mysqli_query($con, "select * from user_followers where from_profile_id = '".$email."' AND follow_status = 1");
        $flwing_user_count = mysqli_num_rows($flwing_user_qry);

        $flwrs_user_qry = mysqli_query($con, "select * from user_followers where to_profile_id = '".$email."' AND follow_status = 1");
        $flwrs_user_count = mysqli_num_rows($flwrs_user_qry);

        $user_details[] = array("first_name" => $first_name, "last_name" => $last_name, "profile_id" => $email, "profile_photo" => $profile_photo, 
        "user_cover_photo" => $user_cover_photo, "flwing_user_count" => $flwing_user_count, "flwrs_user_count" => $flwrs_user_count);

        $query = mysqli_query($con, "select usp.* from user_submitted_photos as usp where usp.profile_id = '".$email."' order by usp.created_date desc");
        $num_rows = mysqli_num_rows($query);

        if($num_rows > 0) {
            $photo_list = array();
            while($rows = mysqli_fetch_array($query)){
                $photo_id = $rows['id'];
                $upload_photo_name = $rows['upload_photo_name'];
                $upload_photo_url = $rows['upload_photo'];
                $created_date = date('l M d  H:i A',strtotime($rows['created_date']));

                //echo "select upr.*, sum(upr.likes) as total_likes from upload_photos_reviews as upr where photo_id = '".$photo_id."' AND profile_id = '".$user_profile_id."' ";

                // Photo Total Likes
                //echo "select sum(upr.likes) as total_likes from upload_photos_reviews as upr where photo_id = '".$photo_id."' AND profile_id = '".$user_profile_id."' ";

                if(str_replace('null','',trim($rows['tags'])) != ""){
                    $tagged_user_name = array();
                    $tagged_profile_ids = explode(',',str_replace(',null','',$rows['tags']));
                    foreach ($tagged_profile_ids as $key => $tagged_user) {
                        //$tagged_user_qry = mysqli_query($con, "select u.first_name, u.last_name from userdetails as u where u.profile_id = '".$tagged_user."' ");
                        $tagged_user_qry = mysqli_query($con, "select u.*, um1.meta_value as user_first_name, um2.meta_value as user_last_name from wpny_users u left join wpny_usermeta um1 on (u.ID = um1.user_id AND um1.meta_key = 'first_name') left join wpny_usermeta um2 on (u.ID = um2.user_id AND um2.meta_key = 'last_name') where user_email = '".$tagged_user."' group by u.ID");
                        $tagged_user_res = mysqli_fetch_array($tagged_user_qry);
                        if($tagged_user_res['user_first_name'] != ""){
                            $tagged_user_name[] = $tagged_user_res['user_first_name'].' '.$tagged_user_res['user_last_name'];
                        }
                    }
                    $tagged_users = implode(', ', $tagged_user_name);
                } else {
                    $tagged_users = "0";
                }


                $photo_rev_query = mysqli_query($con, "select sum(upr.likes) as total_likes from upload_photos_reviews as upr where photo_id = '".$photo_id."'");
                $photo_rev_res = mysqli_fetch_array($photo_rev_query);
                if($photo_rev_res['total_likes'] != ""){ $photo_total_likes = $photo_rev_res['total_likes']; } else { $photo_total_likes = "0"; }

                $current_usr_like_qry = mysqli_query($con, "select * from upload_photos_reviews as upr where photo_id = '".$photo_id."' AND profile_id = '".$email."' AND likes = 1 ");
                $current_usr_res = mysqli_fetch_array($current_usr_like_qry);
                if($current_usr_res['likes'] == 1){ $current_user_like = "You are liked"; } else { $current_user_like = "You are not liked"; }

                // Photo Total Comments
                $photo_commt_query = mysqli_query($con, "select * from upload_photos_comments as upc where photo_id = '".$photo_id."' order by created_date desc");
                $photo_total_commt = mysqli_num_rows($photo_commt_query);

                $comment_list = array();
                while($photo_commt_res = mysqli_fetch_array($photo_commt_query)){
                    $photo_commt_id = $photo_commt_res['id'];
                    $photo_comments = $photo_commt_res['comments'];

                    $comment_list[] = array("photo_commt_id" => $photo_commt_id, "photo_comments" => $photo_comments);
                }

                $photo_list[] = array("photo_id" => $photo_id, "upload_photo_name" => $upload_photo_name, "upload_photo_url" => $upload_photo_url, "created_date" => $created_date, "photo_total_likes" => $photo_total_likes, "photo_total_commt" => $photo_total_commt, "comment_list" => $comment_list, "current_user_like" => $current_user_like,"tagged_users" => $tagged_users);

            }
            $json = array("status" => 1, "msg" => "Photos List", "user_details" => $user_details, "photo_list" => $photo_list);
            header('Content-type: application/json');
            echo json_encode($json);
        } else {

            $json = array("status" => 0, "msg" => "No photos found!", "first_name" => $first_name, "last_name" => $last_name, "email" => $email, "profile_photo" => $profile_photo, "user_cover_photo" => $user_cover_photo, "flwing_user_count" => $flwing_user_count, "flwrs_user_count" => $flwrs_user_count);
            header('Content-type: application/json');
            echo json_encode($json);
        }
        
    } else {
        $json = array("status" => 0, "msg" => "Parameter(s) Missing!");
        header('Content-type: application/json');
        echo json_encode($json);
    }
} else {

    $json = array("status" => 0, "msg" => "Network Error");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>