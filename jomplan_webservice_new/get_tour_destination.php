<?php
include('include/config.php');

if ($con) {

    //$query = mysqli_query($con, "select * from tour_details order by check_in_date asc");
    $sql_str = "SELECT wpt.* FROM wpny_term_taxonomy as wptt INNER JOIN wpny_terms as wpt ON wptt.term_id = wpt.term_id WHERE wptt.taxonomy = 'pa_destination' order by wpt.name asc";

    $query = mysqli_query($con, $sql_str);
    $num_rows = mysqli_num_rows($query);

    if($num_rows > 0){
        $destination_list = array();
        while($rows = mysqli_fetch_array($query)){            
            $destination_id = $rows['term_id'];
            $destination_name = $rows['name'];   

            $tax_meta = "tax_meta_".$destination_id;
            $sql_img_str = "SELECT * FROM wpny_options WHERE option_name = '".$tax_meta."'";
            $sql_img_qry = mysqli_query($con, $sql_img_str);

            if(mysqli_num_rows($sql_img_qry) > 0){
                //$option_value = array();
                $sql_img_rows = mysqli_fetch_array($sql_img_qry);

                $option_value = $sql_img_rows['option_value'];

                //print_r($option_value);
                $str_replace = str_replace('{', '', $option_value);
                $str_replace2 = str_replace('}', '',$str_replace);
                
                $explode_value =  explode(';', $str_replace2);

                //echo '<pre>';
                //print_r($explode_value);
                //echo '</pre>';

                $explode_header_value = $explode_value[15];
                $explode_value_2 =  explode(':', $explode_header_value);
                $image_trim = "http:".trim($explode_value_2[3], '"');
                //$explode_thumb_value = $explode_value[15];
            }

            $destination_list[] = array("destination_id" => $destination_id, "destination_name" => $destination_name, "destination_image" => $image_trim);
        }
        $json = array("status" => 1, "msg" => "Destination List", "destination_list" => $destination_list);
        header('Content-type: application/json');
        echo json_encode($json);
    } else {
        $json = array("status" => 0, "msg" => "No destinations found!");
        header('Content-type: application/json');
        echo json_encode($json);
    }
        
} else {

    $json = array("status" => 0, "msg" => "Network Error");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>