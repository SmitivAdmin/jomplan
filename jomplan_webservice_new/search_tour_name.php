<?php
include('include/config.php');

if ($con) {


    //$query = mysqli_query($con, "select * from tour_details order by check_in_date asc");
    $sql_str = "SELECT wpny_posts.* FROM wpny_term_relationships LEFT JOIN wpny_posts ON wpny_term_relationships.object_id = wpny_posts.ID LEFT JOIN wpny_term_taxonomy ON wpny_term_taxonomy.term_taxonomy_id = wpny_term_relationships.term_taxonomy_id LEFT JOIN wpny_terms ON wpny_terms.term_id = wpny_term_relationships.term_taxonomy_id WHERE wpny_posts.post_type = 'product' AND wpny_posts.post_status = 'publish' AND wpny_term_taxonomy.taxonomy = 'tour_phys' group by wpny_posts.ID order by wpny_posts.post_title asc";

    $query = mysqli_query($con, $sql_str);
    $num_rows = mysqli_num_rows($query);

    if($num_rows > 0){
        $tour_list = array();
        while($rows = mysqli_fetch_array($query)){          
            $tour_id = $rows['ID'];
            $tour_name = $rows['post_title'];   

            $tour_list[] = array("tour_id" => $tour_id, "tour_name" => $tour_name);
        }
        $json = array("status" => 1, "msg" => "Tour List", "tour_list" => $tour_list);
        header('Content-type: application/json');
        echo json_encode($json);
    } else {
        $json = array("status" => 0, "msg" => "No records found!");
        header('Content-type: application/json');
        echo json_encode($json);
    }
        
} else {

    $json = array("status" => 0, "msg" => "Network Error");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>