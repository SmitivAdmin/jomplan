<?php
include('include/config.php');

if ($con) {

    if($_POST["upload_photo"] == "") {
        $json = array("status" => 0, "msg" => "Please pick a image");
        header('Content-type: application/json');
        echo json_encode($json);
        return;
    }
               
    if($_POST["email"] != "" && $_POST["upload_photo_name"] != "" && $_POST["upload_photo"] != "") {
               
        $profile_id = $_POST['email'];
        $upload_photo_name = $_POST['upload_photo_name'];
        $image = $_POST['upload_photo'];
        $latitude = $_POST['latitude'];
        $longitude = $_POST['longitude'];
        $tags=$_POST['tags'];
        $country = $_POST['country'];
        $city = $_POST['city'];

        $data = str_replace('data:image/png;base64,', '', $image);
        $data = str_replace('data:image/jpg;base64,', '', $data);
        $data = str_replace('data:image/jpeg;base64,', '', $data);
        $data = str_replace('data:image/gif;base64,', '', $data);
        $data = str_replace(' ', '+', $data);

        $imageData = base64_decode($data);
        $imageName = 'uploads/' . time() . rand() . '.jpg';
        /*$source = imagecreatefromstring($imageData);
        $imageSave = imagejpeg($source, $imageName, 100);
        imagedestroy($source);*/

        $savetiffile = @file_put_contents($imageName, $imageData);

        $respon['upload_photo'] = $current_url . $imageName;

        $query = mysqli_query($con, "insert into user_submitted_photos set profile_id = '".$profile_id."', 
        	upload_photo_name = '".$upload_photo_name."', latitude = '".$latitude."', tags = '".$tags."',
        	longitude = '".$longitude."', country = '".$country."', city = '".$city."', 
        	upload_photo = '".$respon['upload_photo']."' ");

        if ($query) {

        	$last_id = mysqli_insert_id($con);
			$new_query = "select * from user_submitted_photos where id = '$last_id'";        	
			$new_device_query = mysqli_query($con, $new_query);
			if($new_device_query ->num_rows > 0)  {
         		while($new_row = mysqli_fetch_array($new_device_query))  {
         		
             		if(str_replace('null','',trim($new_row['tags'])) != "") {
                        $tagged_user_name = array();
                        $tagged_profile_ids = explode(',',str_replace(',null','',$new_row['tags']));
                        foreach ($tagged_profile_ids as $key => $tagged_user) {
                            //$tagged_user_qry = mysqli_query($con, "select u.first_name, u.last_name from userdetails as u where u.profile_id = '".$tagged_user."' ");
                            $tagged_user_qry = mysqli_query($con, "select u.*, um1.meta_value as user_first_name, um2.meta_value as user_last_name from wpny_users u left join wpny_usermeta um1 on (u.ID = um1.user_id AND um1.meta_key = 'first_name') left join wpny_usermeta um2 on (u.ID = um2.user_id AND um2.meta_key = 'last_name') where user_email='".$tagged_user."' group by u.ID");
                            $tagged_user_res = mysqli_fetch_array($tagged_user_qry);
                            if($tagged_user_res['user_first_name'] != ""){
                                $tagged_user_name[] = $tagged_user_res['user_first_name'].' '.$tagged_user_res['user_last_name'];
                            }
                        }
                        $tagged_users = implode(', ', $tagged_user_name);
                    } else {
                        $tagged_users = "0";
                    }
                    
                    $photo_submitted_date = $new_row['created_date'];

        		    $created_date =  date('l M d  H:i A',strtotime("$photo_submitted_date + 8 hours"));
        		    $uploaded_url = $new_row["upload_photo"];
         		}
         	}
         			
         	//$user_query = "select * from userdetails where profile_id = '$profile_id'";
            $user_query = "select u.*, um1.meta_value as user_first_name, um2.meta_value as user_last_name from wpny_users u left join wpny_usermeta um1 on (u.ID = um1.user_id AND um1.meta_key = 'first_name') left join wpny_usermeta um2 on (u.ID = um2.user_id AND um2.meta_key = 'last_name') where user_email='".$_POST["email"]."' group by u.ID";

         	$user_get_query = mysqli_query($con, $user_query);
         	if($user_get_query->num_rows > 0)  {
         	   $user_row = mysqli_fetch_array($user_get_query);
     		   $first_name = $user_row["user_first_name"];
     		   $last_name = $user_row["user_last_name"];
     		   $profile_picture = $user_row["imgurl"];
            } 
         			
         	$comment_list = array();
        	$json = array("status" => 1, "msg" => "Photo Uploaded Successfully", 
        	"photo_id" => $last_id, "email" => $profile_id, 
        	"first_name" => $first_name, "last_name" => $last_name, "profile_photo" => $profile_picture,
        	"upload_photo_name" => $upload_photo_name, "upload_photo" => $uploaded_url, 
        	"created_date" => $created_date, "photo_total_likes" => "0", "photo_total_commt" => "0",
        	"comment_list" => $comment_list,"tagged_users" => $tagged_users, 
        	"current_user_like" => "You are not liked", "follow_status" => "0");
            header('Content-type: application/json');
            echo json_encode($json);
        

        } else {

            $json = array("status" => 0, "msg" => "Error Occured");
            header('Content-type: application/json');
            echo json_encode($json);
        }
        
    } else {
        $json = array("status" => 0, "msg" => "Parameters Missing!");
        header('Content-type: application/json');
        echo json_encode($json);
    }
} else {

    $json = array("status" => 0, "msg" => "Network Error");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>