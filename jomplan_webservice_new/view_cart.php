<?php
include('include/config.php');

if ($con) {
    if (($_POST["user_id"] != "")) {

    $user_id = $_POST["user_id"];

    $sql_str = "SELECT * from bookings where user_id = '$user_id'";

    $query = mysqli_query($con, $sql_str);
    $num_rows = mysqli_num_rows($query);

    if($num_rows > 0){
        $cart_list = array();
        while($rows = mysqli_fetch_array($query)){    
            $sno = $rows['sno'];
            $tour_id = $rows['tour_id'];
            $tour_name = $rows['tour_name'];
            $tour_image = $rows['tour_image'];
            $tour_destination = $rows['tour_destination'];
            $user_id = $rows['user_id'];   
            $flights = $rows['flights'];
            $start_date = $rows['tour_start_date'];
            $end_date = $rows['tour_end_date'];
            $adults = $rows['adults'];
            $teens = $rows['teens'];
            $childs = $rows['childs'];
            $infants = $rows['infants'];
            $price = $rows['price'];
            $no_of_persons = $rows['no_of_persons'];
            $sub_total_price = $rows['total_price'];
            $tour_status = $rows['tour_status'];

            $gst_price =  ($sub_total_price * 6) / 100;
            $total_price = $sub_total_price + $gst_price;

            $cart_list[] = array("sno"=>$sno, "tour_id" => $tour_id, "tour_name" => $tour_name, "tour_image" => $tour_image, "tour_destination"=>$tour_destination,"user_id" => $user_id, "flights"=>$flights, "start_date"=>$start_date, "end_date"=>$end_date,"adutls"=>$adults, "teens"=>$teens,"childs"=>$childs,"infants"=>$infants,"price"=>$price,"no_of_persons"=>$no_of_persons,"total_price"=>$total_price,"tour_status"=>$tour_status, "sub_total_price"=>$sub_total_price, "gst_price"=>$gst_price);
        }
        $json = array("status" => 1, "msg" => "Cart List found", "view_cart" => $cart_list);
        header('Content-type: application/json');
        echo json_encode($json);
            } else {    
        $json = array("status" => 0, "msg" => "No data's found");
        header('Content-type: application/json');
        echo json_encode($json);
            }
        } else {
     $json = array("status" => 0, "msg" => "Parameter(s) Missing!");
     header('Content-type: application/json');
     echo json_encode($json);
        }
    } else {
        $json = array("status" => 0, "msg" => "Network Error");
        header('Content-type: application/json');
        echo json_encode($json);
    }
?>