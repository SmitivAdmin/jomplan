<?php
include('include/config.php');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
if ($con) {

$email_id=$_POST['email_id'];
    //$query = mysqli_query($con, "select * from  userdetails where profile_id='$profile_id'");
    $query = mysqli_query($con, "select u.*, um1.meta_value as user_first_name, um2.meta_value as user_last_name from wpny_users u left join wpny_usermeta um1 on (u.ID = um1.user_id AND um1.meta_key = 'first_name') left join wpny_usermeta um2 on (u.ID = um2.user_id AND um2.meta_key = 'last_name') where user_email='".$_POST["email_id"]."' group by u.ID");

    if (mysqli_num_rows($query) > 0) {
        $list = array();
        while($res = mysqli_fetch_array($query)){
            $sno = $res['sno'];
              $first_name = stripcslashes($res['user_first_name']);
              $last_name = stripcslashes($res['user_last_name']);
            $email = $res['user_email'];
            $gender=$res['gender'];
            //$profile_id=$res['profile_id'];
            $dialing_code=$res['dialing_code'];
            $phonenumber=$res['phonenumber'];
            $dob=$res['dob'];
            $country=$res['country'];
            $imgurl=$res['imgurl'];
            $user_cover_photo=$res['user_cover_photo'];

            $followers_query = mysqli_query($con, "select * from user_followers as upr where to_profile_id = '".$email_id."' AND follow_status = '1' ");
            $followers_num_rows = mysqli_num_rows($followers_query);

            $following_query = mysqli_query($con, "select * from user_followers as upr where from_profile_id = '".$email_id."' AND follow_status = '1' ");
            $following_num_rows = mysqli_num_rows($following_query);
            
            $query_photo = mysqli_query($con, "select usp.* from user_submitted_photos as usp where usp.profile_id = '".$email_id."' order by usp.created_date desc");
            $photo_num_rows = mysqli_num_rows($query_photo);
			

            $list[] = array("sno" => $sno,"first_name" =>$first_name,"last_name" =>$last_name,"email" => $email,"gender"=>$gender,
            				"profile_id"=>$profile_id,"dialing_code"=>$dialing_code,"phonenumber"=>$phonenumber,"dob"=>$dob,
            				"country"=>$country,"imgurl"=>$imgurl,"user_cover_photo"=>$user_cover_photo,
            				"followers_count"=>$followers_num_rows,"following_count"=>$following_num_rows, "photos_count"=>$photo_num_rows);
        }

        $json = array("Status" => 1, "Message" => "User Details", "list" => $list);
        //header('Content-type: application/json');
        echo json_encode($json);

    } else {
        $json = array("Status" => 0, "Message" => "No tours found.");
        //header('Content-type: application/json');
        echo json_encode($json);
    }
    
} else {

    $json = array("Status" => 0, "Message" => "Network Error");
    //header('Content-type: application/json');
    echo json_encode($json);
}
?>