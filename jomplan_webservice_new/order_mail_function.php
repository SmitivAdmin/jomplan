<style type="text/css">
#wrapper {
	background-color: #F7F7F7;
	margin: 0;
	padding: 70px 0 70px 0;
	-webkit-text-size-adjust: none !important;
	width: 100%;
}

#template_container {
	box-shadow: 0 1px 4px rgba(0,0,0,0.1) !important;
	background-color: #ffffff;
	border: 1px solid #dedede;
	border-radius: 3px !important;
}

#template_header {
	background-color: #203448;
	border-radius: 3px 3px 0 0 !important;
	color: #ffffff;
	border-bottom: 0;
	font-weight: bold;
	line-height: 100%;
	vertical-align: middle;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
}

#template_header h1,
#template_header h1 a {
	color: #ffffff;
}

#template_footer td {
padding: 0;
-webkit-border-radius: 6px;
}

#template_footer #credit {
border:0;
color: #798591;
font-family: Arial;
font-size:12px;
line-height:125%;
text-align:center;
padding: 0 48px 48px 48px;
}

#body_content {
background-color: #ffffff;
}

#body_content table td {
padding: 48px 48px 0;
}

#body_content table td td {
padding: 12px;
}

#body_content table td th {
padding: 12px;
}

#body_content td ul.wc-item-meta {
font-size: small;
margin: 1em 0 0;
padding: 0;
list-style: none;
}

#body_content td ul.wc-item-meta li {
margin: 0.5em 0 0;
padding: 0;
}

#body_content td ul.wc-item-meta li p {
margin: 0;
}

#body_content p {
margin: 0 0 16px;
}

#body_content_inner {
color: #636363;
font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
font-size: 14px;
line-height: 150%;
text-align: left;
}

.td {
color: #636363;
border: 1px solid #e5e5e5;
}

.address {
padding:12px 12px 0;
color: #636363;
border: 1px solid #e5e5e5;
}

.text {
color: #636363;
font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
}

.link {
color: #15c;
}

#header_wrapper {
padding: 36px 48px;
display: block;
}

h1 {
color: #636363;
font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
font-size: 30px;
font-weight: 300;
line-height: 150%;
margin: 0;
text-align:left;
text-shadow: 0 1px 0 #636363;
-webkit-font-smoothing: antialiased;
}

h2 {
color: #203448;
display: block;
font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
font-size: 18px;
font-weight: bold;
line-height: 130%;
margin: 0 0 18px;
text-align: left;
}

h3 {
color: #203448;
display: block;
font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
font-size: 16px;
font-weight: bold;
line-height: 130%;
margin: 16px 0 8px;
text-align: left;
}

a {
color: #15c;
font-weight: normal;
text-decoration: underline;
}

img {
border: none;
display: inline;
font-size: 14px;
font-weight: bold;
height: auto;
line-height: 100%;
outline: none;
text-decoration: none;
text-transform: capitalize;
}
</style>
<div id="wrapper">
	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
		<tr>
			<td align="center" valign="top">
				<div id="template_header_image"><p style="margin-top:0;"><img src="http://www.jomplan.com/wp-content/uploads/2017/10/logo-1.png" alt="Jomplan" /></p></div>

				<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container">
					<tr>
						<td align="center" valign="top">
							<!-- Header -->
							<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_header">
								<tr>
									<td id="header_wrapper">
										<h1>Thank you for your order</h1>
									</td>
								</tr>
							</table>
							<!-- End Header -->
						</td>
					</tr>
					<tr>
						<td align="center" valign="top">
							<!-- Body -->
							<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body">
								<tr>
									<td valign="top" id="body_content">
										<!-- Content -->
										<table border="0" cellpadding="20" cellspacing="0" width="100%">
											<tr>
												<td valign="top">
													<div id="body_content_inner">
														<p style="margin:0 0 16px">Your order has been received and is now being processed. Your order details are shown below for your reference:</p>
														<p style="margin:0 0 16px">Pay with cash upon delivery.</p>
														<h2>Order #1234</h2>
														<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
															<thead>
															<tr>
																<th class="td" scope="col" style="text-align:left;">Tour</th>
																<th class="td" scope="col" style="text-align:left;">Price</th>
															</tr>
															</thead>
															<tbody>
																<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'order_item', $item, $order ) ); ?>">
																	<td class="td" style="text-align:left; vertical-align:middle; border: 1px solid #eee; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; word-wrap:break-word;">
																	<?php
																	echo $item['name'];
																	echo '<p>Booking Date:' . $date_booking . '</p>';
																	echo '<p>Quantity &times; ' . $item['qty'] . '</p>';
																	?>																	
																	</td>
																	<td class="td" style="text-align:left; vertical-align:middle; border: 1px solid #eee; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;"><?php echo $order->get_formatted_line_subtotal( $item ); ?></td>
															</tr>
															</tbody>
															<tfoot>		
																<tr>
																	<th class="td" scope="row" style="text-align:left; border-top-width: 4px;">Sub Total</th>
																	<td class="td" style="text-align:left; border-top-width: 4px;"><?php echo $total['value']; ?></td>
																</tr>
																<tr>
																	<th class="td" scope="row" style="text-align:left; border-top-width: 4px;">Payment Method</th>
																	<td class="td" style="text-align:left; border-top-width: 4px;"><?php echo $total['value']; ?></td>
																</tr>
																<tr>
																	<th class="td" scope="row" style="text-align:left; border-top-width: 4px;">Total</th>
																	<td class="td" style="text-align:left; border-top-width: 4px;"><?php echo $total['value']; ?></td>
																</tr>
															</tfoot>
														</table>

														<table id="addresses" cellspacing="0" cellpadding="0" style="width: 100%; vertical-align: top; margin-bottom: 40px; padding:0; margin-top: 20px;" border="0">
															<tr>
																<td style="text-align:left; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; border:0; padding:0;" valign="top" width="50%">
																	<h2>Billing address</h2>

																	<address class="address">
																		sasi Kumar<br>
																		12664MKD123<br>
																		11, Test addr<br>
																		Test city<br>
																		Selangor<br>
																		641001<br>
																		788878888<br>
																		sasi@rgmobiles.com
																	</address>
																</td>
															</tr>
														</table>

														<h3 style="border-top:1px solid #AAAAB6; padding-top:13px;">Passport Information:</h3>
														<?php 
															global $wpdb;

														    $passport_info = $wpdb->get_results("SELECT * from tour_user_passport_details where checkout_id = '".$order->id."' AND type = 1 ");
														    $k = 1;

															foreach ( $passport_info as $key => $passport_det ) {
													    	echo '<p><strong>Person '.$k.' Details:</h3> </p>';
													    		echo '<table width="100%" border="1" cellspacing="0">
													    			<tr><th style="text-align:left;">Passport Number</th><td>'.$passport_det->passport_id.'</td></tr>
													    			<tr><th style="text-align:left;">Passport Name</th><td>'.$passport_det->passport_name.'</td></tr>
													    			<tr><th style="text-align:left;">Passport Issue Date</th><td>'.$passport_det->passport_issue_date.'</td></tr>
													    			<tr><th style="text-align:left;">Passport Expiry Date</th><td>'.$passport_det->passport_expiry_date.'</td></tr>
													    			<tr><th style="text-align:left;">Passport Country</th><td>'.$passport_det->passport_country.'</td></tr>
													    			<tr><th style="text-align:left;">Country Number</th><td>'.$passport_det->contact_number.'</td></tr>
													    			<tr><th style="text-align:left;">Date of Birth</th><td>'.$passport_det->birth_date.'</td></tr>
													    		</table>';
													    	$k++;
													    }
														?>
													</div>
												</td>
											</tr>
										</table>
										<!-- End Content -->
									</td>
								</tr>
							</table>
							<!-- End Body -->
						</td>
					</tr>
					<tr>
						<td align="center" valign="top">
							<!-- Footer -->
							<table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer">
								<tr>
									<td valign="top">
										<table border="0" cellpadding="10" cellspacing="0" width="100%">
											<tr>
												<td colspan="2" valign="middle" id="credit">
													<p>Jomplan Sdn Bhd<br><a href="http://jomplan.com/" target="_blank">http://jomplan.com/</a></p>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<!-- End Footer -->
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>