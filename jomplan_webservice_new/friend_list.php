<?php
include('include/config.php');
/*header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");*/
if ($con) {

    $query = mysqli_query($con, "select u.*, um1.meta_value as first_name, um2.meta_value as last_name from wpny_users u left join wpny_usermeta um1 on (u.ID = um1.user_id AND um1.meta_key = 'first_name') left join wpny_usermeta um2 on (u.ID = um2.user_id AND um2.meta_key = 'last_name') group by u.ID");
    if (mysqli_num_rows($query) > 0) {
        $list = array();
        while($res = mysqli_fetch_array($query)){
            $sno = $res['ID'];
            $display_name = stripcslashes($res['display_name']);
            $user_email = stripcslashes($res['user_email']);
            $first_name = stripcslashes($res['first_name']);
            $last_name = stripcslashes($res['last_name']);
            //$content_sec = stripcslashes($res['content_sec']);

            $list[] = array("sno" => $sno, "email"=>$user_email,"first_name"=>$first_name,"last_name"=>$last_name,"display_name"=>$display_name);
        }

        $json = array("Status" => 1, "Message" => "Friends List", "list" => $list);
        header('Content-type: application/json');
        echo json_encode($json);

    } else {
        $json = array("Status" => 0, "Message" => "No friends found.");
        header('Content-type: application/json');
        echo json_encode($json);
    }
    
} else {

    $json = array("Status" => 0, "Message" => "Network Error");
    //header('Content-type: application/json');
    echo json_encode($json);
}
?>