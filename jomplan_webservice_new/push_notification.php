<?php 
function push_notification($deviceToken, $device_type, $msg, $sendername, $alerts_status){

	$deviceToken = $deviceToken;
	$device_type = $device_type;

	$passphrase = 'datify123';

	/* ========= Android ========= */
	if ($device_type == 1) {
		$path_to_fcm = 'http://fcm.googleapis.com/fcm/send';
		$server_key = "AAAAfB5Q7yQ:APA91bEHOCXtYZ8V6gqvfkg1VCzgDKae5lpTpBPbFxmyiSyWYLOvbkOKStuXZckfemTDDm64FU3TBxUyQwq75Ot7Y9EuAcDhvgGYWWYleR0L2sn50rWa4r2wf1xz-uqAfeNJOOtgia_a";

		$message2 = array("status" => 1,"flag" => 1, "title" => "Jomplan", "status_message" => "Success!", "notification_status" => "Notification sent", "name" => $sendername, "message" => $msg, "alerts_status" => $alerts_status);

		$fields1 = array(
		    'registration_ids' => array($deviceToken) ,
		    'data' => $message2,                        
		);

		//print_r($fields1);
		$payload = json_encode($fields1);
		//  echo $payload;
		$headers = array(
		    'Authorization:key=' . $server_key,
		    'Content-Type:application/json'
		);

		$curl_session = curl_init();
		curl_setopt($curl_session, CURLOPT_URL, $path_to_fcm);
		curl_setopt($curl_session, CURLOPT_POST, true);
		curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl_session, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl_session, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		curl_setopt($curl_session, CURLOPT_POSTFIELDS, $payload);

		$result1 = curl_exec($curl_session);
		if (curl_error($curl_session)) {
		//  echo 'error:' . curl_error($curl_session);
		    $status_msg =  'Message not delivered';
		    $status = 0;
		} else{
		//  echo $result1;
		    $status_msg =  'Message successfully delivered';
		    $status = 1;
		}
		curl_close($curl_session);
	}
	/* ========= Android ========= */

	/* ========= IOS ========= */
	if ($device_type == 2) {

		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

		// Open a connection to the APNS server
		$fp = stream_socket_client(
		        'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
		// $fp = stream_socket_client(
		// 'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

		if (!$fp){
		    exit("Failed to connect: $err $errstr" . PHP_EOL);
		}
		//echo 'Connected to APNS' . PHP_EOL;
		// Create the payload body
		$sm = 'Jomplan: ' . $msg;
		$body = array();
		$body['aps'] = array(
		    'alert' => $sm,
		    'sound' => 'default'
		);

		$body['person'] = array(
		    'name' => $sendername,
		    'flag' => 1,
		    'message' => $sm
		);

		// Encode the payload as JSON
		//print_r($body);
		$payload = json_encode($body);

		// echo $payload;
		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));
		// echo $result;
		if (!$result){
		    //echo 'Message not delivered' . PHP_EOL. $deviceToken;
		    $status_msg =  'Message not delivered';
		    $status = 0;
		} else {
		    //echo 'Message successfully delivered' . PHP_EOL. $deviceToken;
		    $status_msg =  'Message successfully delivered';
		    $status = 1;
		}
		 
		// Close the connection to the server
		fclose($fp);
	}
	/* ========= IOS ========= */

	return $status_msg;
}
?>