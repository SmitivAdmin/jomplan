<?php
include('include/config.php');
if ($con) {
    if (($_POST["email"] != "")) {

        //$query = mysqli_query($con, "select * from wpny_users where user_email='".$_POST["email"]."' ");
        $query = mysqli_query($con, "select u.*, um1.meta_value as user_first_name, um2.meta_value as user_last_name from wpny_users u left join wpny_usermeta um1 on (u.ID = um1.user_id AND um1.meta_key = 'first_name') left join wpny_usermeta um2 on (u.ID = um2.user_id AND um2.meta_key = 'last_name') where user_email='".$_POST["email"]."' group by u.ID");

        $res = mysqli_fetch_array($query);
        $num_rows = mysqli_num_rows($query);

        if($num_rows > 0){            
            $json = array("status" => 1, "msg" => "View Details", "first_name" => $res['user_first_name'], "last_name" => $res['user_last_name'], "email" => $res['user_email'], "profile_image" => $res['imgurl'], "user_cover_photo" => $res['user_cover_photo'], "country" => $res['country'], "dialing_code" => $res['dialing_code'], "phonenumber" => $res['phonenumber']);
            header('Content-type: application/json');
            echo json_encode($json);
        } else {
             $json = array("status" => 0, "msg" => "No Details!");
            header('Content-type: application/json');
            echo json_encode($json);
        }

    } else {
        $json = array("status" => 0, "msg" => "Pass email id");
        header('Content-type: application/json');
        echo json_encode($json);
    }
} else {

    $json = array("status" => 0, "msg" => "Network Error");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>