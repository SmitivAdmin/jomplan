<?php
include('include/config.php');
if ($con) {
    if ($_POST["email"] != "") {

        $view_user_query = mysqli_query($con, "select * from wpny_users where user_email='".$_POST["email"]."' ");
        $view_user_res = mysqli_fetch_array($view_user_query);

        $user_id = $view_user_res["ID"];

        $first_name = $_POST["first_name"];
        $last_name = $_POST["last_name"];
        $email = $_POST["email"];
        $password = $_POST["password"];
        $image = $_POST["imgurl"];
        $phonenumber = $_POST["phonenumber"];
        $user_cover_photo = $_POST["user_cover_photo"];
        $country = $_POST['country'];
        $dialing_code = $_POST['dialing_code'];

        if($password != ""){
            $edit_password = " password = '".md5($password)."', ";
            $user_pass = " user_pass = '".md5($password)."', ";
        }

        if($image != ""){
            $data = str_replace('data:image/png;base64,', '', $image);
            $data = str_replace('data:image/jpg;base64,', '', $data);
            $data = str_replace('data:image/jpeg;base64,', '', $data);
            $data = str_replace('data:image/gif;base64,', '', $data);
            $data = str_replace(' ', '+', $data);

            $imageData = base64_decode($data);
            $imageName = 'uploads/' . time() . rand() . '.jpg';

            /*$source = imagecreatefromstring($imageData);
            $imageSave = imagejpeg($source, $imageName, 100);
            imagedestroy($source);*/
            $savetiffile = @file_put_contents($imageName, $imageData);
            
            $respon['profile_photo'] = $current_url . $imageName;

            $profile_photo = " imgurl = '".$respon['profile_photo']."', ";
        } else {
            $respon['profile_photo'] = $view_user_res['imgurl'];
        } 
        if($user_cover_photo != ""){
            $data2 = str_replace('data:image/png;base64,', '', $user_cover_photo);
            $data2 = str_replace('data:image/jpg;base64,', '', $data2);
            $data2 = str_replace('data:image/jpeg;base64,', '', $data2);
            $data2 = str_replace('data:image/gif;base64,', '', $data2);
            $data2 = str_replace(' ', '+', $data2);

            $imageData2 = base64_decode($data2);
            $imageName2 = 'uploads/' . time() . rand() . '.jpg';

            /*$source2 = imagecreatefromstring($imageData2);
            $imageSave2 = imagejpeg($source2, $imageName2, 100);
            imagedestroy($source2);*/
            $savetiffile = @file_put_contents($imageName2, $imageData2);

            $respon['cover_photo'] = $current_url . $imageName2;

            $cover_photo = " user_cover_photo = '".$respon['cover_photo']."', ";
        } else {
            $respon['cover_photo'] = $view_user_res['user_cover_photo'];
        }

        //$query = mysqli_query($con, "update userdetails set first_name = '".$first_name."', last_name = '".$last_name."', email = '".$email."',  ".$edit_password." ".$profile_photo." ".$cover_photo." phonenumber = '".$phonenumber."', country = '".$country."', dialing_code = '".$dialing_code."' WHERE profile_id = '".$_POST["profile_id"]."' ");

        $display_name = $first_name.' '.$last_name;

        $current_date = date('Y-m-d H:i:s');

        $query = mysqli_query($con, "UPDATE wpny_users set profile_id = '".$profile_id."', first_name = '".$first_name."', last_name = '".$last_name."', email = '".$email."', ".$edit_password." ".$user_pass." ".$profile_photo." ".$cover_photo." phonenumber = '".$phonenumber."', country = '".$country."', dialing_code = '".$dialing_code."', user_nicename = '".strtolower($first_name)."', display_name = '".$display_name."' WHERE user_email = '".$_POST["email"]."' ");

        if ($query) {

            $update_query = mysqli_query($con, "UPDATE wpny_usermeta set meta_value = '".$first_name."' WHERE user_id = '".$user_id."' AND meta_key = 'first_name' ");
            $update_query = mysqli_query($con, "UPDATE wpny_usermeta set meta_value = '".$last_name."' WHERE user_id = '".$user_id."' AND meta_key = 'last_name' ");
            $update_query = mysqli_query($con, "UPDATE wpny_usermeta set meta_value = '".$first_name."' WHERE user_id = '".$user_id."' AND meta_key = 'nickname' ");
            $update_query = mysqli_query($con, "UPDATE wpny_usermeta set meta_value = '".strtotime($current_date)."' WHERE user_id = '".$user_id."' AND meta_key = 'last_update' ");

            $json = array("status" => 1, "msg" => "Details are Updated!", "first_name" => $first_name, "last_name" => $last_name, "email" => $email, "profile_image" => $respon['profile_photo'], "user_cover_photo" => $respon['cover_photo'], "country" => $country, "dialing_code" => $dialing_code, "phonenumber" => $phonenumber);
            header('Content-type: application/json');
            echo json_encode($json);
        } else {

            $json = array("status" => 0, "msg" => "Error Occured");
            header('Content-type: application/json');
            echo json_encode($json);
        }
    } else {
        $json = array("status" => 0, "msg" => "Pass Profile Id!");
        header('Content-type: application/json');
        echo json_encode($json);
    }
} else {

    $json = array("status" => 0, "msg" => "Network Error");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>