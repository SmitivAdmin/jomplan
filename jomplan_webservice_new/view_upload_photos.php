<?php
include('include/config.php');

if ($con) {
    if (($_POST["email"] != "")) {

        $email = $_POST['email'];

        //$query = mysqli_query($con, "select usp.*, u.first_name, u.last_name, u.imgurl from user_submitted_photos as usp inner join userdetails as u on u.profile_id = usp.profile_id where usp.profile_id != '".$profile_id."' order by usp.created_date desc");

        //$login_user_query = mysqli_query($con, "select u.* from userdetails as u where u.profile_id = '".$profile_id."' ");
        $login_user_query = mysqli_query($con, "select u.*, um1.meta_value as user_first_name, um2.meta_value as user_last_name from wpny_users u left join wpny_usermeta um1 on (u.ID = um1.user_id AND um1.meta_key = 'first_name') left join wpny_usermeta um2 on (u.ID = um2.user_id AND um2.meta_key = 'last_name') where user_email='".$_POST["email"]."' group by u.ID");

        $login_user_res = mysqli_fetch_array($login_user_query);

        $login_user_details = array("login_user_email" => $email, "login_user_first_name" => $login_user_res['user_first_name'], "login_user_last_name" => $login_user_res['user_last_name'], "login_user_profile_photo" => $login_user_res['imgurl']);

        //$query = mysqli_query($con, "select usp.*, u.first_name, u.last_name, u.imgurl from user_submitted_photos as usp inner join userdetails as u on u.profile_id = usp.profile_id order by usp.created_date desc");

        $query = mysqli_query($con, "select usp.*, u.imgurl, u.user_email, u.display_name, um1.meta_value as user_first_name, um2.meta_value as user_last_name from wpny_users u left join wpny_usermeta um1 on (u.ID = um1.user_id AND um1.meta_key = 'first_name') left join wpny_usermeta um2 on (u.ID = um2.user_id AND um2.meta_key = 'last_name') inner join user_submitted_photos usp on u.user_email = usp.profile_id group by u.ID order by usp.created_date desc");

        $num_rows = mysqli_num_rows($query);

        if($num_rows > 0){
            $photo_list = array();
            while($rows = mysqli_fetch_array($query)){

                if($rows['user_first_name'] != ""){
                    $user_first_name = $rows['user_first_name'];
                    $user_last_name = $rows['user_last_name'];
                } else {
                    $user_first_name = $rows['display_name'];
                    $user_last_name = "";
                }
                
                $user_email_id = $rows['user_email'];
                $profile_photo = $rows['imgurl'];

                $photo_id = $rows['id'];
                $upload_photo_name = $rows['upload_photo_name'];
                $upload_photo_url = $rows['upload_photo'];
                $created_date = date('l M d  H:i A',strtotime($rows['created_date']));

                if(str_replace('null','',trim($rows['tags'])) != ""){
                    $tagged_user_name = array();
                    $tagged_profile_ids = explode(',',str_replace(',null','',$rows['tags']));
                    foreach ($tagged_profile_ids as $key => $tagged_user) {
                        //$tagged_user_qry = mysqli_query($con, "select u.first_name, u.last_name from userdetails as u where u.profile_id = '".$tagged_user."' ");
                        $tagged_user_qry = mysqli_query($con, "select u.*, um1.meta_value as user_first_name, um2.meta_value as user_last_name from wpny_users u left join wpny_usermeta um1 on (u.ID = um1.user_id AND um1.meta_key = 'first_name') left join wpny_usermeta um2 on (u.ID = um2.user_id AND um2.meta_key = 'last_name') where user_email = '".$tagged_user."' group by u.ID");
                        $tagged_user_res = mysqli_fetch_array($tagged_user_qry);
                        if($tagged_user_res['user_first_name'] != ""){
                            $tagged_user_name[] = $tagged_user_res['user_first_name'].' '.$tagged_user_res['user_last_name'];
                        }
                    }
                    $tagged_users = implode(', ', $tagged_user_name);
                } else {
                    $tagged_users = "0";
                }
                

                //echo "select upr.*, sum(upr.likes) as total_likes from upload_photos_reviews as upr where photo_id = '".$photo_id."' AND profile_id = '".$user_email_id."' ";

                // Photo Total Likes
                //echo "select sum(upr.likes) as total_likes from upload_photos_reviews as upr where photo_id = '".$photo_id."' AND profile_id = '".$user_email_id."' ";

                $photo_rev_query = mysqli_query($con, "select sum(upr.likes) as total_likes from upload_photos_reviews as upr where photo_id = '".$photo_id."'");
                $photo_rev_res = mysqli_fetch_array($photo_rev_query);
                if($photo_rev_res['total_likes'] != ""){ $photo_total_likes = $photo_rev_res['total_likes']; } else { $photo_total_likes = "0"; }

                $current_usr_like_qry = mysqli_query($con, "select * from upload_photos_reviews as upr where photo_id = '".$photo_id."' AND profile_id = '".$email."' AND likes = 1 ");
                $current_usr_res = mysqli_fetch_array($current_usr_like_qry);

                if($current_usr_res['likes'] == 1){ $current_user_like = "You are liked"; } else { $current_user_like = "You are not liked"; }
                
                //Get Follow Status
                $follow_status = 0;
                $follow_sts_qry = mysqli_query($con, "select * from user_followers as uf where from_profile_id = '".$email."' AND to_profile_id = '".$user_email_id."' AND follow_status = 1 ");
                $follow_usr_res = mysqli_fetch_array($follow_sts_qry);
                if($follow_usr_res['follow_status'] == 1){ $follow_status = "1"; } else { $follow_status = "0"; }

                // Photo Total Comments
                $photo_commt_query = mysqli_query($con, "select * from upload_photos_comments as upc where photo_id = '".$photo_id."' order by created_date desc");
                $photo_total_commt = mysqli_num_rows($photo_commt_query);

                $comment_list = array();
                while($photo_commt_res = mysqli_fetch_array($photo_commt_query)){
                    $photo_commt_id = $photo_commt_res['id'];
                    $photo_comments = $photo_commt_res['comments'];

                    $comment_list[] = array("photo_commt_id" => $photo_commt_id, "photo_comments" => $photo_comments);
                }

                $photo_list[] = array("photo_id" => $photo_id, "user_first_name" => $user_first_name, "user_last_name" => $user_last_name, "user_email_id" => $user_email_id, "profile_photo" => $profile_photo, "upload_photo_name" => $upload_photo_name, "upload_photo_url" => $upload_photo_url, "created_date" => $created_date, "photo_total_likes" => $photo_total_likes, "photo_total_commt" => $photo_total_commt, "comment_list" => $comment_list, "current_user_like" => $current_user_like, "tagged_users" => $tagged_users, "follow_status" => $follow_status);

            }
            $json = array("status" => 1, "msg" => "Photos List", "login_user_details" => $login_user_details, "photo_list" => $photo_list);
            header('Content-type: application/json');
            echo json_encode($json);
        } else {
            $json = array("status" => 0, "msg" => "No photos found!");
            header('Content-type: application/json');
            echo json_encode($json);
        }
        
    } else {
        $json = array("status" => 0, "msg" => "Parameter(s) Missing!");
        header('Content-type: application/json');
        echo json_encode($json);
    }
} else {

    $json = array("status" => 0, "msg" => "Network Error");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>