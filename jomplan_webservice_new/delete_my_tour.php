<?php
include('include/config.php');
if ($con) {

    if (($_POST["checkout_id"] != "") && ($_POST["user_id"] != "") ) {

        $checkout_id = $_POST["checkout_id"];
        $user_id = $_POST["user_id"];

        //$query = mysqli_query($con, "DELETE FROM tour_checkout WHERE id = '$checkout_id' and profile_id = '$user_id'");

        $del_order_query = mysqli_query($con, "DELETE FROM wpny_posts WHERE ID = '".$checkout_id."' ");
        $del_order_meta_query = mysqli_query($con, "DELETE FROM wpyg_postmeta WHERE post_id = '".$checkout_id."' ");
        
        $select_order_item_query = mysqli_query($con, "SELECT * FROM wpny_woocommerce_order_items WHERE order_id = '".$checkout_id."' ");
        while($select_order_item_rows = mysqli_fetch_array($select_order_item_query)){
            $del_order_item_meta_query = mysqli_query($con, "DELETE FROM wpny_woocommerce_order_itemmeta WHERE order_item_id = '".$select_order_item_rows['order_item_id']."' ");
        }
        $del_order_item_query = mysqli_query($con, "DELETE FROM wpny_woocommerce_order_items WHERE order_id = '".$checkout_id."' ");

        if ($del_order_query) {
            $json = array("status" => 1, "msg" => "Successfully removed.");
        	header('Content-type: application/json');
        	echo json_encode($json);
         } else {
            $json = array("status" => 0, "msg" => "Not removed, please check later");
	        header('Content-type: application/json');
    	    echo json_encode($json);
    	}
    } else {
		$json = array("status" => 0, "msg" => "Parameter(s) Missing!");
		header('Content-type: application/json');
		echo json_encode($json);
	}
} else {
	$json = array("status" => 0, "msg" => "Network Error");
	header('Content-type: application/json');
	echo json_encode($json);
}
?>