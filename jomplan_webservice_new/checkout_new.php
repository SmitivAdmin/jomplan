<?php
include('include/config.php');
if ($con) {
    /*if (($_POST["tour_id"] != "") && ($_POST["user_email"] != "") && ($_POST["flights"] != "") && ($_POST["travel_from"] != "") && ($_POST["travel_to"] != "") && ($_POST["tour_date"] != "") && ($_POST["return_date"] != "") && ($_POST["payment_status"] != "") && ($_POST["tour_price_per_person"] != "") && ($_POST["tour_total_price"] != "") && ($_POST["passport_number"] != "") && ($_POST["passport_id"] != "")&& ($_POST["passport_name"] != "")&& ($_POST["passport_issue_date"] != "")&& ($_POST["passport_expiry_date"] != "") ) {*/

    if (($_POST["sno"] != "") && ($_POST["tour_id"] != "") && ($_POST["user_email"] != "") && ($_POST["flights"] != "") && ($_POST["tour_date"] != "") && ($_POST["return_date"] != "") && ($_POST["tour_price_per_person"] != "") && ($_POST["tour_total_price"] != "") && ($_POST["sub_total_price"] != "") && ($_POST["gst_price"] != "")) {

        $sno = $_POST["sno"];
        $tour_id = $_POST["tour_id"];
        $user_email = $_POST["user_email"];
        $flights = $_POST["flights"]; // 1 = One Way, 2 = Round Trip
        /*$travel_from = $_POST["travel_from"];
        $travel_to = $_POST["travel_to"];*/
        $tour_date = $_POST["tour_date"]; // Date = YYYY-mm-dd
        $return_date = $_POST["return_date"]; // Date = YYYY-mm-dd
        $adults = $_POST["adults"];
        $teens = $_POST["teens"];
        $childrens = $_POST["childrens"];
        $infants = $_POST["infants"];
        $tour_price_per_person = $_POST["tour_price_per_person"];        

        $sub_total_price = $_POST["sub_total_price"];
        $gst_price = $_POST["gst_price"];

/*        $passport_number=$_POST["passport_number"];
        $passport_id=$_POST["passport_id"];
        $passport_name=$_POST["passport_name"];
        $passport_issue_date=$_POST["passport_issue_date"];
        $passport_expiry_date=$_POST["passport_expiry_date"];
        $tour_status = $_POST["tour_status"];*/

        $sel_user_query = mysqli_query($con, "select * from wpny_users where user_email = '".$user_email."' ");
        $sel_user_res = mysqli_fetch_array($sel_user_query);

        $tour_sql_str = "SELECT wpny_posts.* FROM wpny_posts LEFT JOIN wpny_term_relationships ON wpny_term_relationships.object_id = wpny_posts.ID LEFT JOIN wpny_term_taxonomy ON wpny_term_taxonomy.term_taxonomy_id = wpny_term_relationships.term_taxonomy_id LEFT JOIN wpny_terms ON wpny_terms.term_id = wpny_term_relationships.term_taxonomy_id WHERE wpny_posts.ID = '".$tour_id."' ";
        $tour_query = mysqli_query($con, $tour_sql_str);
        $tour_rows = mysqli_fetch_array($tour_query);

        if($flights == 1){
            $flights_trip = "One Way";
        } else {
            $flights_trip = "Round Trip";
        }

        if($adults != "" || $adults != 0 || $teens != "" || $teens != 0 || $childrens != "" || $childrens != 0 || $infants != "" || $infants != 0){

            //$query = mysqli_query($con, "insert into tour_checkout set tour_id = '".$tour_id."', user_email = '".$user_email."', flights = '".$flights."', travel_from = '".$travel_from."', travel_to = '".$travel_to."', tour_date = '".$tour_date."', return_date = '".$return_date."', adults = '".$adults."', teens = '".$teens."', childrens = '".$childrens."', infants = '".$infants."', tour_status = '".$tour_status."',passport_number = '".$passport_number."',passport_id = '".$passport_id."',passport_name = '".$passport_name."',passport_issue_date = '".$passport_issue_date."',passport_expiry_date = '".$passport_expiry_date."' ");
                
            //$query = mysqli_query($con, "insert into tour_checkout set tour_id = '".$tour_id."', profile_id = '".$profile_id."', flights = '".$flights."', tour_date = '".$tour_date."', return_date = '".$return_date."', adults = '".$adults."', teens = '".$teens."', childrens = '".$childrens."', infants = '".$infants."', tour_price_per_person = '".$tour_price_per_person."', tour_total_price = '".$tour_total_price."', sub_total_price = '".$sub_total_price."', gst_price = '".$gst_price."' ");

            $current_date = date('Y-m-d H:i:s');
            $post_title = "Order &ndash; ".date('F d, Y @ H:i A');
            $post_name = strtolower("order-".date('M-d-Y-Hi-a'));
            $unique_id = uniqid('wc_order_');

            $qty = 0;
            if($adults != ""){
                $qty +=  $adults;
            }
            if($teens != ""){
                $qty +=  $teens;
            }
            if($childrens != ""){
                $qty +=  $childrens;
            }
            if($infants != ""){
                $qty +=  $infants;
            }

            $tour_total_price = $_POST["tour_price_per_person"] * $qty;

            $query = mysqli_query($con, "insert into wpny_posts set post_author = '1', post_date = '".$current_date."', post_date_gmt = '".$current_date."', post_title = '".$post_title."', post_name = '".$post_name."', post_modified = '".$current_date."', post_modified_gmt = '".$current_date."', post_type = 'shop_order', post_status = 'wc-pending'");

            if ($query) {

                $order_id = mysqli_insert_id($con);    

                $guid_url = $site_url."?post_type=shop_order&p=".$order_id;

                /*$reference_no = 100000 + $last_id;
                $update_query = mysqli_query($con, "update tour_checkout set reference_no = '".$reference_no."' where id = '".$last_id."' ");*/
                
                $update_query = mysqli_query($con, "update wpny_posts set guid = '".$guid_url."'");

                $user_id = $sel_user_res['ID'];

                $post_meta_query = mysqli_query($con, "insert into wpny_postmeta (post_id,meta_key,meta_value) VALUES
                    ('".$order_id."', '_order_key', '".$unique_id."'),
                    ('".$order_id."', '_customer_user', '".$user_id."'),
                    ('".$order_id."', 'flights', '".$flights."'),
                    ('".$order_id."', '_created_via', 'checkout'),
                    ('".$order_id."', '_date_completed', ''),
                    ('".$order_id."', '_completed_date', ''),
                    ('".$order_id."', '_date_paid', ''),
                    ('".$order_id."', '_paid_date', ''),
                    ('".$order_id."', '_order_currency', 'MYR'),
                    ('".$order_id."', '_cart_discount', '0'),
                    ('".$order_id."', '_cart_discount_tax', '0'),
                    ('".$order_id."', '_order_shipping', '0'),
                    ('".$order_id."', '_order_shipping_tax', '0'),
                    ('".$order_id."', '_order_tax', '".$gst_price."'),
                    ('".$order_id."', '_order_total', '".$tour_total_price."'),
                    ('".$order_id."', '_order_version', '3.2.3'),
                    ('".$order_id."', '_prices_include_tax', '0')
                ");


                $order_item_name = $tour_rows['post_title'];
                $order_item_query_1 = mysqli_query($con, "insert into wpny_woocommerce_order_items set order_item_name = '".$order_item_name."', order_item_type = 'line_item', order_id = '".$order_id."'");

                if($order_item_query_1){
                    $order_item_id_1 = mysqli_insert_id($con);

                    //$line_tax_data = 'a:2:{s:5:"total";a:1:{i:1;s:2:"'.$gst_price.'";}s:8:"subtotal";a:1:{i:1;s:2:"'.$gst_price.'";}}';
                    $line_tax_data = '';

                    $order_item_meta_query = mysqli_query($con, "insert into wpny_woocommerce_order_itemmeta (order_item_id,meta_key,meta_value) VALUES 
                        ('".$order_item_id_1."', '_product_id', '".$tour_id."'),
                        ('".$order_item_id_1."', '_variation_id', '0'),
                        ('".$order_item_id_1."', '_qty', '".$qty."'),
                        ('".$order_item_id_1."', '_line_subtotal', '".$sub_total_price."'),
                        ('".$order_item_id_1."', '_line_subtotal_tax', '".$gst_price."'),
                        ('".$order_item_id_1."', '_line_total', '".$sub_total_price."'),
                        ('".$order_item_id_1."', '_line_tax', '".$gst_price."'),
                        ('".$order_item_id_1."', '_line_tax_data', '".$line_tax_data."'),
                        ('".$order_item_id_1."', '_date_booking', '".$tour_date."'),
                        ('".$order_item_id_1."', '_return_date', '".$return_date."')
                    ");
                }


                $order_item_query_2 = mysqli_query($con, "insert into wpny_woocommerce_order_items set order_item_name = 'Flat rate', order_item_type = 'shipping', order_id = '".$order_id."'");
                if($order_item_query_2){

                    $order_item_id_2 = mysqli_insert_id($con);

                    //$taxes = 'a:1:{s:5:"total";a:0:{}}';
                    $taxes = '';
                    $items = $order_item_name.'&times;'.$qty;
                    $order_item_meta_query = mysqli_query($con, "insert into wpny_woocommerce_order_itemmeta (order_item_id,meta_key,meta_value) VALUES 
                        ('".$order_item_id_2."', 'method_id', 'flat_rate:1'),
                        ('".$order_item_id_2."', 'cost', '0.0'),
                        ('".$order_item_id_2."', 'total_tax', '0'),
                        ('".$order_item_id_2."', 'taxes', '".$taxes."'),
                        ('".$order_item_id_2."', 'Items', '".$gst_price."'),
                        ('".$order_item_id_2."', 'vendor_id', '".$sub_total_price."'),
                        ('".$order_item_id_2."', 'package_qty', '".$gst_price."')
                    ");
                }


                $order_item_query_3 = mysqli_query($con, "insert into wpny_woocommerce_order_items set order_item_name = 'MY-GST-1', order_item_type = 'tax', order_id = '".$order_id."'");
                if($order_item_query_3){
                    $order_item_id_3 = mysqli_insert_id($con);

                    //$taxes = 'a:1:{s:5:"total";a:0:{}}';
                    $taxes = '';
                    $items = $order_item_name.' &times; '.$qty;
                    $order_item_meta_query = mysqli_query($con, "insert into wpny_woocommerce_order_itemmeta (order_item_id,meta_key,meta_value) VALUES 
                        ('".$order_item_id_3."', 'rate_id', '1'),
                        ('".$order_item_id_3."', 'label', 'GST'),
                        ('".$order_item_id_3."', 'compound', ''),
                        ('".$order_item_id_3."', 'tax_amount', '".$gst_price."'),
                        ('".$order_item_id_3."', 'shipping_tax_amount', '0')
                    ");
                }



                $sel_query = mysqli_query($con, "select * from bookings where sno = '".$sno."' ");
                $num_rows = mysqli_num_rows($sel_query);
            
                if($num_rows > 0){
            		while($rows = mysqli_fetch_array($sel_query)){          		 
                		$del_query = mysqli_query($con, "DELETE FROM bookings WHERE sno = '".$sno."' and user_id = '".$user_email."' ");
                		
                		$json = array("status" => 1, "msg" => "Payment Successful, We will send confirmation soon.", "checkout_id" => $order_id);
                        header('Content-type: application/json');
                        echo json_encode($json);
            		}
        	    }

            } else {
                $json = array("status" => 0, "msg" => "Error Occured");
                header('Content-type: application/json');
                echo json_encode($json);
            }

        } else {

            $json = array("status" => 0, "msg" => "Any one correct value enter in travellers field.");
            header('Content-type: application/json');
            echo json_encode($json);

        }

    } else {
        $json = array("status" => 0, "msg" => "Parameter(s) Missing!");
        header('Content-type: application/json');
        echo json_encode($json);
    }
} else {

    $json = array("status" => 0, "msg" => "Network Error");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>