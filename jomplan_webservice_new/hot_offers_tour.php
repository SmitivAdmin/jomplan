<?php
include('include/config.php');

if ($con) {

    $email = $_POST["email"];
    //$user_query = mysqli_query($con, "select * from userdetails where profile_id='".$profile_id."' ");
    $user_query = mysqli_query($con, "select u.*, um1.meta_value as user_first_name, um2.meta_value as user_last_name from wpny_users u left join wpny_usermeta um1 on (u.ID = um1.user_id AND um1.meta_key = 'first_name') left join wpny_usermeta um2 on (u.ID = um2.user_id AND um2.meta_key = 'last_name') where user_email='".$_POST["email"]."' group by u.ID");
    $user_res = mysqli_fetch_array($user_query);

    if($user_res['imgurl'] != ""){ $profile_image = $user_res['imgurl']; } else { $profile_image = ""; }

    //$query = mysqli_query($con, "select * from tour_details order by check_in_date asc");
   // $sql_str = "SELECT wpny_posts.*, wpny_terms.name as destination FROM wpny_term_relationships LEFT JOIN wpny_posts ON wpny_term_relationships.object_id = wpny_posts.ID LEFT JOIN wpny_term_taxonomy ON wpny_term_taxonomy.term_taxonomy_id = wpny_term_relationships.term_taxonomy_id LEFT JOIN wpny_terms ON wpny_terms.term_id = wpny_term_relationships.term_taxonomy_id WHERE wpny_posts.post_type = 'product' AND wpny_term_taxonomy.taxonomy = 'tour_phys' group by wpny_posts.ID";
    $sql_str = "SELECT wpny_posts.*, wpny_terms.name as destination FROM wpny_term_relationships LEFT JOIN wpny_posts ON wpny_term_relationships.object_id = wpny_posts.ID LEFT JOIN wpny_term_taxonomy ON wpny_term_taxonomy.term_taxonomy_id = wpny_term_relationships.term_taxonomy_id LEFT JOIN wpny_terms ON wpny_terms.term_id = wpny_term_relationships.term_taxonomy_id LEFT JOIN wpny_postmeta pm7 ON (pm7.post_id = wpny_posts.ID AND pm7.meta_key = '_tour_end_date') WHERE wpny_posts.post_type = 'product' AND wpny_term_taxonomy.taxonomy = 'product_type' AND wpny_terms.slug = 'tour_phys' AND wpny_posts.post_status = 'publish' AND STR_TO_DATE(pm7.meta_value,'%m/%d/%Y') >= NOW() group by wpny_posts.ID";
    $query = mysqli_query($con, $sql_str);
    $num_rows = mysqli_num_rows($query);

    if($num_rows > 0){
        $tour_list = array();
        while($rows = mysqli_fetch_array($query)){            

            $tour_id = $rows['ID'];
            $tour_name = $rows['post_title']; 

            $dest_sql_str = "SELECT wpny_terms.name as destination FROM wpny_terms LEFT JOIN wpny_term_taxonomy on wpny_term_taxonomy.term_id = wpny_terms.term_id LEFT JOIN wpny_term_relationships on wpny_term_relationships.term_taxonomy_id = wpny_term_taxonomy.term_id WHERE wpny_term_relationships.object_id = '".$tour_id."' AND wpny_term_taxonomy.taxonomy = 'pa_destination'";
            $dest_query = mysqli_query($con, $dest_sql_str);
            $destination_array = array();
            while($dest_rows = mysqli_fetch_array($dest_query)){
                $destination_array[] = $dest_rows['destination'];
            }
            $destination = implode(', ',$destination_array);
            
             $traveltype_sql_str = "SELECT wpt.name as tourtype FROM wpny_term_taxonomy as wptt LEFT JOIN wpny_terms as wpt ON wptt.term_id = wpt.term_id LEFT JOIN wpny_term_relationships on wpny_term_relationships.term_taxonomy_id = wptt.term_id WHERE wpny_term_relationships.object_id = '".$tour_id."' AND wptt.taxonomy = 'tour_phys'";
            $tourtype_query = mysqli_query($con, $traveltype_sql_str);
            $tourtype_array = array();
            while($tourtype_rows = mysqli_fetch_array($tourtype_query)){
                $tourtype_array[] = $tourtype_rows['tourtype'];
            }
            $tourtype = implode(', ',$tourtype_array);
            

            $post_meta_sql_str = "SELECT
            pm1.meta_value AS tour_code,
            pm2.meta_value AS regular_price,
            pm3.meta_value AS sale_price,
            pm4.meta_value AS price,
            pm5.meta_value AS tour_duration,
            pm6.meta_value AS start_date,
            pm7.meta_value AS end_date,
            pm8.meta_value AS minimum_person,
            pm9.meta_value AS thumbnail_id,
            pm10.meta_value AS destination,
            pm11.meta_value AS tour_latitude,
            pm12.meta_value AS tour_longitude
            FROM wpny_posts p
            LEFT JOIN wpny_postmeta pm1 ON (pm1.post_id = p.ID AND pm1.meta_key = '_tour_code')
            LEFT JOIN wpny_postmeta pm2 ON (pm2.post_id = p.ID AND pm2.meta_key = '_regular_price')
            LEFT JOIN wpny_postmeta pm3 ON (pm3.post_id = p.ID AND pm3.meta_key = '_sale_price')
            LEFT JOIN wpny_postmeta pm4 ON (pm4.post_id = p.ID AND pm4.meta_key = '_price')
            LEFT JOIN wpny_postmeta pm5 ON (pm5.post_id = p.ID AND pm5.meta_key = '_tour_duration')
            LEFT JOIN wpny_postmeta pm6 ON (pm6.post_id = p.ID AND pm6.meta_key = '_tour_start_date')
            LEFT JOIN wpny_postmeta pm7 ON (pm7.post_id = p.ID AND pm7.meta_key = '_tour_end_date')
            LEFT JOIN wpny_postmeta pm8 ON (pm8.post_id = p.ID AND pm8.meta_key = '_tour_number_ticket')  
            LEFT JOIN wpny_postmeta pm9 ON (pm9.post_id = p.ID AND pm9.meta_key = '_thumbnail_id') 
            LEFT JOIN wpny_postmeta pm10 ON (pm10.post_id = p.ID AND pm10.meta_key = '_tour_location_address') 
            LEFT JOIN wpny_postmeta pm11 ON (pm11.post_id = p.ID AND pm11.meta_key = '_tour_location_lat') 
            LEFT JOIN wpny_postmeta pm12 ON (pm12.post_id = p.ID AND pm12.meta_key = '_tour_location_long')     
            WHERE p.ID  = '".$tour_id."' ";

            $post_meta_qry = mysqli_query($con, $post_meta_sql_str);
            $post_meta_res = mysqli_fetch_array($post_meta_qry);

            $image_queryMeta=mysqli_query($con,"SELECT * FROM wpny_postmeta WHERE meta_key='_wp_attached_file' AND post_id='".$post_meta_res['thumbnail_id']."'");
            $image_res = mysqli_fetch_array($image_queryMeta);
            $tour_image=$product_image_url.$image_res['meta_value'];

            //$tour_image = $post_meta_res['tour_image'];         
            $minimum_person = $post_meta_res['minimum_person'];
            $tour_price = 'RM'.number_format($post_meta_res['price'],2);
            $regular_price = 'RM'.number_format($post_meta_res['regular_price'],2);
            //$destination = $post_meta_res['destination'];
            $duration = $post_meta_res['tour_duration'];
            $check_in_date = date('d/m/Y', strtotime($post_meta_res['start_date']));
            $check_out_date = date('l, d M Y', strtotime($post_meta_res['end_date']));

            $tour_list[] = array("tour_id" => $tour_id, "tour_name" => $tour_name, "tour_image" => $tour_image, "check_in_date" => $check_in_date, "minimum_person" => $minimum_person, "tour_price" => $tour_price, "regular_price" => $regular_price, "destination" => $destination, "tour_types" => $tourtype, "duration" => $duration, "check_out_date" => $check_out_date);
        }
        $json = array("status" => 1, "msg" => "Tour List", "email" => $email, "profile_image" => $profile_image, "tour_list" => $tour_list);
        header('Content-type: application/json');
        echo json_encode($json);
    } else {
        $json = array("status" => 0, "msg" => "No tours found!");
        header('Content-type: application/json');
        echo json_encode($json);
    }
        
} else {

    $json = array("status" => 0, "msg" => "Network Error");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>