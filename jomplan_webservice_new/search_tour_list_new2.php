<?php
include('include/config.php');

if ($con) {

    if($_POST['destination'] != ""){
        $where_dest = " AND wpny_terms.name like '%".$_POST['destination']."%' ";
    } else{
        $where_dest = "";
    }

    /*if($_POST['check_in_date'] != ""){
        $check_in_date = date('Y-m-d',strtotime($_POST['check_in_date']));
        $where_start_date = " AND pm6.meta_value like '%".$_POST['check_in_date']."%' ";
    } else{
        $where_start_date = "";
    }*/

    if($_POST['budget'] != ""){
        $budget = explode('-',$_POST['budget']);
        $where_budget_search = " AND pm4.meta_value >= ".trim($budget[0])." AND pm4.meta_value <= ".trim($budget[1])." ";
    } else{
        $where_budget_search = "";
    }

    if($_POST['check_out_date'] != ""){
        $check_out_date = $_POST['check_out_date'];
        $where_end_date = " AND pm7.meta_value <= '".$check_out_date."' ";
    } else{
        $where_end_date = "";
    }

    if($_POST['search_tour'] != ""){
        $search_tour = $_POST['search_tour'];
        $where_search_tour = " AND wpny_posts.post_title like '%".$search_tour."%' ";
    } else{
        $where_search_tour = "";
    }

    if($_POST['tour_type'] != ""){
        $where_tour_type = " AND wpny_term_taxonomy.taxonomy = 'tour_phys' AND wpny_terms.name like '%".$_POST['tour_type']."%' ";
    } else{
        $where_tour_type = "";
    }

    //$query = mysqli_query($con, "select * from tour_details order by check_in_date asc");
    echo $sql_str = "SELECT wpny_posts.* FROM wpny_term_relationships LEFT JOIN wpny_posts ON wpny_term_relationships.object_id = wpny_posts.ID LEFT JOIN wpny_term_taxonomy ON wpny_term_taxonomy.term_taxonomy_id = wpny_term_relationships.term_taxonomy_id LEFT JOIN wpny_terms ON wpny_terms.term_id = wpny_term_relationships.term_taxonomy_id WHERE wpny_posts.post_type = 'product' AND wpny_term_taxonomy.taxonomy = 'product_type' AND wpny_terms.slug = 'tour_phys' ".$where_search_tour." ".$where_dest." ".$where_tour_type." group by wpny_posts.ID order by wpny_posts.post_title asc";

    $query = mysqli_query($con, $sql_str);
    $num_rows = mysqli_num_rows($query);

    if($num_rows > 0){
        $tour_list = array();
        while($rows = mysqli_fetch_array($query)){

            $tour_id = $rows['ID'];
            $tour_name = $rows['post_title'];   

            $dest_sql_str = "SELECT wpny_terms.name as destination FROM wpny_terms LEFT JOIN wpny_term_taxonomy on wpny_term_taxonomy.term_id = wpny_terms.term_id LEFT JOIN wpny_term_relationships on wpny_term_relationships.term_taxonomy_id = wpny_term_taxonomy.term_id WHERE wpny_term_relationships.object_id = '".$tour_id."' AND wpny_term_taxonomy.taxonomy = 'pa_destination'";
            $dest_query = mysqli_query($con, $dest_sql_str);
            $destination_array = array();
            while($dest_rows = mysqli_fetch_array($dest_query)){
                $destination_array[] = $dest_rows['destination'];
            }
            $destination = implode(', ',$destination_array);
            
             $traveltype_sql_str = "SELECT wpt.name as tourtype FROM wpny_term_taxonomy as wptt LEFT JOIN wpny_terms as wpt ON wptt.term_id = wpt.term_id LEFT JOIN wpny_term_relationships on wpny_term_relationships.term_taxonomy_id = wptt.term_id WHERE wpny_term_relationships.object_id = '".$tour_id."' AND wptt.taxonomy = 'tour_phys'";
            $tourtype_query = mysqli_query($con, $traveltype_sql_str);
            $tourtype_array = array();
            while($tourtype_rows = mysqli_fetch_array($tourtype_query)){
                $tourtype_array[] = $tourtype_rows['tourtype'];
            }
            $tourtype = implode(', ',$tourtype_array);
            

            $post_meta_sql_str = "SELECT
            pm1.meta_value AS tour_code,
            pm2.meta_value AS regular_price,
            pm3.meta_value AS sale_price,
            pm4.meta_value AS price,
            pm5.meta_value AS tour_duration,
            pm6.meta_value AS start_date,
            pm7.meta_value AS end_date,
            pm8.meta_value AS minimum_person,
            pm9.meta_value AS thumbnail_id,
            pm10.meta_value AS destination,
            pm11.meta_value AS tour_latitude,
            pm12.meta_value AS tour_longitude
            FROM wpny_posts p
            LEFT JOIN wpny_postmeta pm1 ON (pm1.post_id = p.ID AND pm1.meta_key = '_tour_code')
            LEFT JOIN wpny_postmeta pm2 ON (pm2.post_id = p.ID AND pm2.meta_key = '_regular_price')
            LEFT JOIN wpny_postmeta pm3 ON (pm3.post_id = p.ID AND pm3.meta_key = '_sale_price')
            LEFT JOIN wpny_postmeta pm4 ON (pm4.post_id = p.ID AND pm4.meta_key = '_price')
            LEFT JOIN wpny_postmeta pm5 ON (pm5.post_id = p.ID AND pm5.meta_key = '_tour_duration')
            LEFT JOIN wpny_postmeta pm6 ON (pm6.post_id = p.ID AND pm6.meta_key = '_tour_start_date')
            LEFT JOIN wpny_postmeta pm7 ON (pm7.post_id = p.ID AND pm7.meta_key = '_tour_end_date')
            LEFT JOIN wpny_postmeta pm8 ON (pm8.post_id = p.ID AND pm8.meta_key = '_tour_number_ticket')  
            LEFT JOIN wpny_postmeta pm9 ON (pm9.post_id = p.ID AND pm9.meta_key = '_thumbnail_id') 
            LEFT JOIN wpny_postmeta pm10 ON (pm10.post_id = p.ID AND pm10.meta_key = '_tour_location_address') 
            LEFT JOIN wpny_postmeta pm11 ON (pm11.post_id = p.ID AND pm11.meta_key = '_tour_location_lat') 
            LEFT JOIN wpny_postmeta pm12 ON (pm12.post_id = p.ID AND pm12.meta_key = '_tour_location_long')     
            WHERE p.ID  = '".$tour_id."' ".$where_budget_search." ";

            $post_meta_qry = mysqli_query($con, $post_meta_sql_str);
            $post_meta_res = mysqli_fetch_array($post_meta_qry);
            $post_meta_num_rows = mysqli_num_rows($post_meta_qry);

            $start_date = date('Y-m-d',strtotime($post_meta_res['start_date']));
            $end_date = date('Y-m-d',strtotime($post_meta_res['end_date']));
            $post_check_in_date = date('Y-m-d',strtotime($_POST['check_in_date']));
            $aDays = array();
            while($start_date < $end_date){  
               // Add a day to the current date  
               $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));  

               // Add this new day to the aDays array  
               $aDays[] = $start_date;  
            }
            /*if(in_array($post_check_in_date, $aDays)){
                echo "find";
            } */

            if($_POST['check_in_date'] != ""){
                if(in_array($post_check_in_date, $aDays) && $post_meta_res['start_date'] != "" && $post_meta_res['end_date'] != ""){                    
                    $image_queryMeta=mysqli_query($con,"SELECT * FROM wpny_postmeta WHERE meta_key='_wp_attached_file' AND post_id='".$post_meta_res['thumbnail_id']."'");
                    $image_res = mysqli_fetch_array($image_queryMeta);
                    $tour_image=$product_image_url.$image_res['meta_value'];

                    //$tour_image = $post_meta_res['tour_image'];         
                    $minimum_person = $post_meta_res['minimum_person'];                
                    $tour_price = 'RM'.number_format($post_meta_res['price'],2);
                    $regular_price = 'RM'.number_format($post_meta_res['regular_price'],2);
                    //$destination = $post_meta_res['destination'];
                    $duration = $post_meta_res['tour_duration'];
                    $check_in_date = $post_meta_res['start_date'];
                    $check_out_date = date('l, d M Y', strtotime($post_meta_res['end_date']));                    
                
                    $tour_list[] = array("tour_id" => $tour_id, "tour_name" => $tour_name, "tour_image" => $tour_image, "minimum_person" => $minimum_person, "tour_price" => $tour_price, "regular_price" => $regular_price, "destination" => $destination, "tour_types" => $tourtype,"duration" => $duration, "check_in_date" => $check_in_date, "check_out_date" => $check_out_date);
                }

            } else {
                if($post_meta_num_rows > 0){
                    
                    $image_queryMeta=mysqli_query($con,"SELECT * FROM wpny_postmeta WHERE meta_key='_wp_attached_file' AND post_id='".$post_meta_res['thumbnail_id']."'");
                    $image_res = mysqli_fetch_array($image_queryMeta);
                    $tour_image=$product_image_url.$image_res['meta_value'];

                    //$tour_image = $post_meta_res['tour_image'];         
                    $minimum_person = $post_meta_res['minimum_person'];                
                    $tour_price = 'RM'.number_format($post_meta_res['price'],2);
                    $regular_price = 'RM'.number_format($post_meta_res['regular_price'],2);
                    //$destination = $post_meta_res['destination'];
                    $duration = $post_meta_res['tour_duration'];
                    $check_in_date = $post_meta_res['start_date'];
                    $check_out_date = date('l, d M Y', strtotime($post_meta_res['end_date']));

                    $tour_list[] = array("tour_id" => $tour_id, "tour_name" => $tour_name, "tour_image" => $tour_image, "minimum_person" => $minimum_person, "tour_price" => $tour_price, "regular_price" => $regular_price, "destination" => $destination,"tour_types" => $tourtype , "duration" => $duration, "check_in_date" => $check_in_date, "check_out_date" => $check_out_date);

                    $i = 1;
                }
            }

        }

        $json = array("status" => 1, "msg" => "Tour List", "tour_list" => $tour_list);
        header('Content-type: application/json');
        echo json_encode($json);
    } else {
        $json = array("status" => 0, "msg" => "No tours found!");
        header('Content-type: application/json');
        echo json_encode($json);
    }
        
} else {

    $json = array("status" => 0, "msg" => "Network Error");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>