<?php
include('include/config.php');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
if ($con) {


    $query = mysqli_query($con, "select * from about ");
    if (mysqli_num_rows($query) > 0) {
        $list = array();
        while($res = mysqli_fetch_array($query)){
            $id = $res['id'];
              $title_fi = stripcslashes($res['title_fi']);
               $content_fi = stripcslashes($res['content_fi']);
               
                $title_sec = stripcslashes($res['title_sec']);
               $content_sec = stripcslashes($res['content_sec']);
           

            $list[] = array("id" => $id,"title_fi" =>$title_fi,"content_fi"=>$content_fi,"title_sec" =>$title_sec,"content_sec"=>$content_sec);
        }

        $json = array("Status" => 1, "Message" => "About US", "list" => $list);
        //header('Content-type: application/json');
        echo json_encode($json);

    } else {
        $json = array("Status" => 0, "Message" => "No tours found.");
        //header('Content-type: application/json');
        echo json_encode($json);
    }
    
} else {

    $json = array("Status" => 0, "Message" => "Network Error");
    //header('Content-type: application/json');
    echo json_encode($json);
}
?>