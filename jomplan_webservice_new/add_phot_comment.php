<?php
include('include/config.php');

if ($con) {
    if (($_POST["user_email_id"] != "") && ($_POST["photo_id"] != "") && ($_POST["comments"] != "")) {

        $user_email_id = $_POST['user_email_id'];
        $photo_id = $_POST['photo_id'];
        $comments = addslashes($_POST['comments']); 

        $user_query = mysqli_query($con, "select * from wpny_users where user_email='".$user_email_id."' ");
        $user_res = mysqli_fetch_array($user_query);

        if($user_res['imgurl'] != ""){ $profile_image = $user_res['imgurl']; } else { $profile_image = ""; }

        $photo_rev_query = mysqli_query($con, "insert into upload_photos_comments set comments = '".$comments."', photo_id = '".$photo_id."', profile_id = '".$user_email_id."' ");
        
        if($photo_rev_query){

            $get_notf_users_qry = mysqli_query($con, "SELECT a.user_login AS from_user_name, b.*
            FROM wpny_users a, wpny_users b
            INNER JOIN user_submitted_photos usp ON usp.profile_id = b.user_email
            WHERE a.user_email = '".$user_email_id."'
            AND usp.id = '".$photo_id."'");
        
            //echo  "select a.fullname as from_user_name, b.* from dating_register a, dating_register b where a.fb_id = '".$from_fb_id."' AND (b.fb_id = '".$to_fb_id."' AND  b.alerts like '%Request%')";

            if(mysqli_num_rows($get_notf_users_qry) > 0){
              
              //echo mysqli_num_rows($get_notf_users_qry) > 0;

              $get_notf_users_res = mysqli_fetch_array($get_notf_users_qry);

              $msg = $get_notf_users_res['from_user_name'] . " commented on a your photo.";
              $sendername = $get_notf_users_res['from_user_name'];

              if($get_notf_users_res["fcm_token"] != ""){
                  $deviceToken = $get_notf_users_res["fcm_token"];
                  $device_type = 1;
                  $alerts_status = "photo_comment";

                  //$push_msg = push_notification($deviceToken, $device_type, $msg, $sendername, $alerts_status);

                    $path_to_fcm = 'http://fcm.googleapis.com/fcm/send';
                    $server_key = "AAAAfB5Q7yQ:APA91bEHOCXtYZ8V6gqvfkg1VCzgDKae5lpTpBPbFxmyiSyWYLOvbkOKStuXZckfemTDDm64FU3TBxUyQwq75Ot7Y9EuAcDhvgGYWWYleR0L2sn50rWa4r2wf1xz-uqAfeNJOOtgia_a";

                    $message2 = array("status" => 1,"flag" => 1, "title" => "Jomplan", "status_message" => "Success!", "notification_status" => "Notification sent", "name" => $sendername, "message" => $msg, "alerts_status" => $alerts_status);

                    $fields1 = array(
                        'registration_ids' => array($deviceToken) ,
                        'data' => $message2,                        
                    );

                    //print_r($fields1);
                    $payload = json_encode($fields1);
                    //  echo $payload;
                    $headers = array(
                        'Authorization:key=' . $server_key,
                        'Content-Type:application/json'
                    );

                    $curl_session = curl_init();
                    curl_setopt($curl_session, CURLOPT_URL, $path_to_fcm);
                    curl_setopt($curl_session, CURLOPT_POST, true);
                    curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curl_session, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($curl_session, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
                    curl_setopt($curl_session, CURLOPT_POSTFIELDS, $payload);

                    $result1 = curl_exec($curl_session);
                    if (curl_error($curl_session)) {
                    //  echo 'error:' . curl_error($curl_session);
                        $push_msg =  'Message not delivered';
                        $status = 0;
                    } else{
                    //  echo $result1;
                        $push_msg =  'Message successfully delivered';
                        $status = 1;
                    }
                    curl_close($curl_session);

              } else {
                 $push_msg = "Device Token Empty!";
              }

            }

            $json = array("status" => 1, "msg" => "Comment added.", "user_email_id" => $user_email_id, "photo_id" => $photo_id, "comments" => $comments, "profile_image" => $profile_image, "push_msg" => $push_msg);
            header('Content-type: application/json');
            echo json_encode($json);
        } else{
            $json = array("status" => 0, "msg" => "Error Occured");
            header('Content-type: application/json');
            echo json_encode($json);
        }

    } else {
        $json = array("status" => 0, "msg" => "Parameter(s) Missing!");
        header('Content-type: application/json');
        echo json_encode($json);
    }
} else {

    $json = array("status" => 0, "msg" => "Network Error");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>