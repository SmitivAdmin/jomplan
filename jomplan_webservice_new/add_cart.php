<?php
include('include/config.php');
if ($con) {

    if (($_POST["tour_id"] != "") && ($_POST["user_id"] != "") && ($_POST["flights"] != "") && ($_POST["start_date"] != "") && ($_POST["end_date"] != "") && ($_POST["adults"] != "") && ($_POST["teens"] != "") && ($_POST["childs"] != "") && ($_POST["infants"] != "") && ($_POST["price"] != "")  && ($_POST["no_of_persons"] != "")  && ($_POST["total_price"] != "")  &&($_POST["tour_status"] != "") && ($_POST["tour_name"] != "") && ($_POST["tour_image"] != "")) {
        
        // && ($_POST["tour_destination"] != "")

        $tour_name = $_POST["tour_name"];
        $tour_image = $_POST["tour_image"];
        $tour_destination = $_POST["tour_destination"];
        $tour_id = $_POST["tour_id"];
        $user_id = $_POST["user_id"];
        $flights = $_POST["flights"]; 
        $start_date = $_POST["start_date"];
        $end_date = $_POST["end_date"];
        $adults = $_POST["adults"];
        $teens = $_POST["teens"];
        $childs = $_POST["childs"];
        $infants = $_POST["infants"];
        $price = $_POST["price"];
        $no_of_persons = $_POST["no_of_persons"];
        $total_price = $_POST["total_price"];
        $tour_status = $_POST["tour_status"];

        $query = mysqli_query($con, "insert into bookings set tour_id = '".$tour_id."', user_id = '".$user_id."', flights = '".$flights."', tour_start_date = '".$start_date."', tour_end_date = '".$end_date."', adults = '".$adults."', teens = '".$teens."', childs = '".$childs."', infants = '".$infants."', price = '".$price."', no_of_persons = '".$no_of_persons."', total_price = '".$total_price."', tour_status = '".$tour_status."',  tour_name = '".$tour_name."', tour_image = '".$tour_image."', tour_destination = '".$tour_destination."' ");

        if ($query) {

            $json = array("status" => 1, "msg" => "Tour booked successfully.");
            header('Content-type: application/json');
            echo json_encode($json);
        } else {
            $json = array("status" => 0, "msg" => "Error Occured");
            header('Content-type: application/json');
            echo json_encode($json);
        }

    } else {
        $json = array("status" => 0, "msg" => "Parameter(s) Missing!");
        header('Content-type: application/json');
        echo json_encode($json);
    }
} else {

    $json = array("status" => 0, "msg" => "Network Error");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>