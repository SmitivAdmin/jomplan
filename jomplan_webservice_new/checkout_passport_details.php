<?php
include('include/config.php');
if ($con) {

    if (($_POST["tour_id"] != "") && ($_POST["email_id"] != "") && ($_POST["passport_id"] != "") && ($_POST["passport_name"] != "") &&
     ($_POST["passport_issue_date"] != "") && ($_POST["passport_expiry_date"] != "") && ($_POST["passport_country"] != "") && 
     ($_POST["checkout_id"] != "") && ($_POST["passport_contact"] != "") && ($_POST["passport_dob"] != "") && ($_POST["f_name"] != "") && 
     ($_POST["l_name"] != "") && ($_POST["email"] != "") && ($_POST["phone"] != "") && 
     ($_POST["street"] != "") && ($_POST["city"] != "") && ($_POST["state"] != "") && ($_POST["postcode"] != "") &&
     ($_POST["country"] != "") ) {
        
        $f_name = $_POST["f_name"];
        $l_name = $_POST["l_name"];
        $email = $_POST["email"];
        $phone = $_POST["phone"];
        $company_name = $_POST["company_name"];
        $street = $_POST["street"];
        $city = $_POST["city"];
        $state = $_POST["state"];
        $postcode = $_POST["postcode"];
        $country = $_POST["country"];
        
        $tour_id = $_POST["tour_id"];
        $email_id = $_POST["email_id"];
        $checkout_id = $_POST["checkout_id"];
        $passport_country=$_POST["passport_country"];
        $passport_id=$_POST["passport_id"];
        $passport_name=$_POST["passport_name"];
        $passport_contact=$_POST["passport_contact"];
        $passport_dob=$_POST["passport_dob"];
        $passport_issue_date=$_POST["passport_issue_date"];
        $passport_expiry_date=$_POST["passport_expiry_date"];

        $query = mysqli_query($con, "insert into tour_user_passport_details set tour_id = '".$tour_id."', profile_id = '".$email_id."', checkout_id = '".$checkout_id."', passport_country = '".$passport_country."',passport_id = '".$passport_id."',passport_name = '".$passport_name."',passport_issue_date = '".$passport_issue_date."',passport_expiry_date = '".$passport_expiry_date."', contact_number = '".$passport_contact."', birth_date = '".$passport_dob."' , b_first_name = '".$f_name."', b_last_name = '".$l_name."', b_email = '".$email."', b_phone = '".$phone."', b_company_name = '".$company_name."', b_street = '".$street."', b_city = '".$city."', b_state = '".$state."', b_postcode = '".$postcode."', b_country = '".$country."'");


        if ($query) {

            $order_id = $checkout_id;

            $billing_address_index = $f_name.' '.$l_name.' '.$company_name.' '.$street.' '.$city.' '.$state.' '.$postcode.' '.$country.' '.$email.' '.$phone;
            $post_meta_query = mysqli_query($con, "insert into wpyg_postmeta (post_id,meta_key,meta_value) VALUES
                ('".$order_id."', '_billing_first_name', '".$f_name."'),
                ('".$order_id."', '_billing_last_name', '".$l_name."'),
                ('".$order_id."', '_billing_company', '".$company_name."'),
                ('".$order_id."', '_billing_address_1', '".$street."'),
                ('".$order_id."', '_billing_address_2', ''),
                ('".$order_id."', '_billing_city', '".$city."'),
                ('".$order_id."', '_billing_state', '".$state."'),
                ('".$order_id."', '_billing_postcode', '".$postcode."'),
                ('".$order_id."', '_billing_country', '".$country."'),
                ('".$order_id."', '_billing_email', '".$email."'),
                ('".$order_id."', '_billing_phone', '".$phone."'),
                ('".$order_id."', '_shipping_first_name', '".$f_name."'),
                ('".$order_id."', '_shipping_last_name', '".$l_name."'),
                ('".$order_id."', '_shipping_company', '".$company_name."'),
                ('".$order_id."', '_shipping_address_1', '".$street."'),
                ('".$order_id."', '_shipping_address_2', ''),
                ('".$order_id."', '_shipping_city', '".$city."'),
                ('".$order_id."', '_shipping_state', '".$state."'),
                ('".$order_id."', '_shipping_postcode', '".$postcode."'),
                ('".$order_id."', '_shipping_country', '".$country."'),
                ('".$order_id."', '_billing_address_index', '".$billing_address_index."'),
                ('".$order_id."', '_shipping_address_index', '".$billing_address_index."'),
                ('".$order_id."', '_wcmp_order_processed', '1')
            ");

            //$shipping_method = 'a:1:{i:0;s:11:"flat_rate:1";}';
            $shipping_method = '';
            
            $userquery = mysqli_query($con, "SELECT u.*, um.billing_company from wpny_users u 
            LEFT JOIN wpny_usermeta um um.user_id = u.ID AND meta_key = 'billing_first_name'
            where user_email='".$email_id."' ");
            $user_data_rows = mysqli_fetch_array($userquery);
            $user_id = $user_data_rows['ID'];

            if($user_data_rows['billing_company'] != ""){
                $user_meta_query_1 = mysqli_query($con, "update wpny_usermeta SET meta_value = '".$f_name."' WHERE user_id = '".$user_id."' AND meta_key = 'billing_first_name'");
                $user_meta_query_2 = mysqli_query($con, "update wpny_usermeta SET meta_value = '".$l_name."' WHERE user_id = '".$user_id."' AND meta_key = 'billing_last_name'");
                $user_meta_query_3 = mysqli_query($con, "update wpny_usermeta SET meta_value = '".$company_name."' WHERE user_id = '".$user_id."' AND meta_key = 'billing_company'");
                $user_meta_query_4 = mysqli_query($con, "update wpny_usermeta SET meta_value = '".$street."' WHERE user_id = '".$user_id."' AND meta_key = 'billing_address_1'");
                $user_meta_query_5 = mysqli_query($con, "update wpny_usermeta SET meta_value = '' WHERE user_id = '".$user_id."' AND meta_key = 'billing_address_2'");
                $user_meta_query_6 = mysqli_query($con, "update wpny_usermeta SET meta_value = '".$city."' WHERE user_id = '".$user_id."' AND meta_key = 'billing_city'");
                $user_meta_query_7 = mysqli_query($con, "update wpny_usermeta SET meta_value = '".$state."' WHERE user_id = '".$user_id."' AND meta_key = 'billing_state'");
                $user_meta_query_8 = mysqli_query($con, "update wpny_usermeta SET meta_value = '".$postcode."' WHERE user_id = '".$user_id."' AND meta_key = 'billing_postcode'");
                $user_meta_query_9 = mysqli_query($con, "update wpny_usermeta SET meta_value = '".$country."' WHERE user_id = '".$user_id."' AND meta_key = 'billing_country'");
                $user_meta_query_10 = mysqli_query($con, "update wpny_usermeta SET meta_value = '".$email."' WHERE user_id = '".$user_id."' AND meta_key = 'billing_email'");
                $user_meta_query_11 = mysqli_query($con, "update wpny_usermeta SET meta_value = '".$phone."' WHERE user_id = '".$user_id."' AND meta_key = 'billing_phone'");
                
                $user_meta_query_12 = mysqli_query($con, "update wpny_usermeta SET meta_value = '".$f_name."' WHERE user_id = '".$user_id."' AND meta_key = 'shipping_first_name'");
                $user_meta_query_13 = mysqli_query($con, "update wpny_usermeta SET meta_value = '".$l_name."' WHERE user_id = '".$user_id."' AND meta_key = 'shipping_last_name'");
                $user_meta_query_14 = mysqli_query($con, "update wpny_usermeta SET meta_value = '".$company_name."' WHERE user_id = '".$user_id."' AND meta_key = 'shipping_company'");
                $user_meta_query_15 = mysqli_query($con, "update wpny_usermeta SET meta_value = '".$street."' WHERE user_id = '".$user_id."' AND meta_key = 'shipping_address_1'");
                $user_meta_query_16 = mysqli_query($con, "update wpny_usermeta SET meta_value = '' WHERE user_id = '".$user_id."' AND meta_key = 'shipping_address_2'");
                $user_meta_query_17 = mysqli_query($con, "update wpny_usermeta SET meta_value = '".$city."' WHERE user_id = '".$user_id."' AND meta_key = 'shipping_city'");
                $user_meta_query_18 = mysqli_query($con, "update wpny_usermeta SET meta_value = '".$state."' WHERE user_id = '".$user_id."' AND meta_key = 'shipping_state'");
                $user_meta_query_19 = mysqli_query($con, "update wpny_usermeta SET meta_value = '".$postcode."' WHERE user_id = '".$user_id."' AND meta_key = 'shipping_postcode'");
                $user_meta_query_20 = mysqli_query($con, "update wpny_usermeta SET meta_value = '".$country."' WHERE user_id = '".$user_id."' AND meta_key = 'shipping_country'");
            } else {
                $user_meta_query = mysqli_query($con, "insert into wpny_usermeta (user_id,meta_key,meta_value) VALUES
                    ('".$user_id."', 'billing_first_name', '".$f_name."'),
                    ('".$user_id."', 'billing_last_name', '".$l_name."'),
                    ('".$user_id."', 'billing_company', '".$company_name."'),
                    ('".$user_id."', 'billing_address_1', '".$street."'),
                    ('".$user_id."', 'billing_address_2', ''),
                    ('".$user_id."', 'billing_city', '".$city."'),
                    ('".$user_id."', 'billing_state', '".$state."'),
                    ('".$user_id."', 'billing_postcode', '".$postcode."'),
                    ('".$user_id."', 'billing_country', '".$country."'),
                    ('".$user_id."', 'billing_email', '".$email."'),
                    ('".$user_id."', 'billing_phone', '".$phone."'),
                    ('".$user_id."', 'shipping_first_name', '".$f_name."'),
                    ('".$user_id."', 'shipping_last_name', '".$l_name."'),
                    ('".$user_id."', 'shipping_company', '".$company_name."'),
                    ('".$user_id."', 'shipping_address_1', '".$street."'),
                    ('".$user_id."', 'shipping_address_2', ''),
                    ('".$user_id."', 'shipping_city', '".$city."'),
                    ('".$user_id."', 'shipping_state', '".$state."'),
                    ('".$user_id."', 'shipping_postcode', '".$postcode."'),
                    ('".$user_id."', 'shipping_country', '".$country."'),
                    ('".$user_id."', 'paying_customer', '".$user_id."'),
                    ('".$user_id."', 'shipping_method', '".$shipping_method."')
                ");
            }

            $json = array("status" => 1, "msg" => "Passport Details Added Successfully.");
            header('Content-type: application/json');
            echo json_encode($json);
        } else {
            $json = array("status" => 0, "msg" => "Error Occured");
            header('Content-type: application/json');
            echo json_encode($json);
        }

    } else {
        $json = array("status" => 0, "msg" => "Parameter(s) Missing!");
        header('Content-type: application/json');
        echo json_encode($json);
    }
} else {

    $json = array("status" => 0, "msg" => "Network Error");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>