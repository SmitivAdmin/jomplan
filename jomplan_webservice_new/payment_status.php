<?php
include('include/config.php');
if ($con) {

    if (($_POST["checkout_id"] != "") && ($_POST["tour_id"] != "") && ($_POST["profile_id"] != "") && ($_POST["payment_status"] != "") ) {

        $checkout_id = $_POST["checkout_id"];
        $profile_id = $_POST["profile_id"];
        $tour_id = $_POST["tour_id"];
        $tour_status = $_POST["payment_status"];


        if($tour_status == 1){
            $suc_message = "Payment Successful, We will send confirmation soon.";
        } else {
            $suc_message = "Payment Transaction Error.";
        }

        $query = mysqli_query($con, "update tour_checkout set tour_status = '".$tour_status."' where id = '".$checkout_id."' ");

        $get_checkout_query = mysqli_query($con, "select * tour_checkout where id = '".$checkout_id."' ");
        $get_checkout_res = mysqli_fetch_array($get_checkout_query);

        if ($query) {

            if($tour_status == 1){

                $get_user_query = mysqli_query($con, "select * from userdetails WHERE profile_id = '".$profile_id."' ");
                $get_user_res = mysqli_fetch_array($get_user_query);

                //$get_tour_query = mysqli_query($con, "select * from tour_details WHERE id = '".$tour_id."' ");
                 $tour_sql_str = "SELECT wpny_posts.* FROM wpny_term_relationships LEFT JOIN wpny_posts ON wpny_term_relationships.object_id = wpny_posts.ID LEFT JOIN wpny_term_taxonomy ON wpny_term_taxonomy.term_taxonomy_id = wpny_term_relationships.term_taxonomy_id LEFT JOIN wpny_terms ON wpny_terms.term_id = wpny_term_relationships.term_taxonomy_id WHERE wpny_posts.post_type = 'product' AND wpny_term_taxonomy.taxonomy = 'tour_phys' AND wpny_posts.ID = '".$tour_id."' group by wpny_posts.ID";
                $get_tour_query = mysqli_query($con, $tour_sql_str);
                $get_tour_res = mysqli_fetch_array($get_tour_query);

                $tour_name = $get_tour_res['post_title'];   
                $reference_no = $get_checkout_res['reference_no'];
                if($get_checkout_res['flights'] == 1){
                    $flights_trip = "One Way";
                } else {
                    $flights_trip = "Round Trip";
                }
                $depature_date = $get_checkout_res['tour_date'];
                $return_date = $get_checkout_res['return_date'];
                $adults = $get_checkout_res['adults'];
                $teens = $get_checkout_res['teens'];
                $childrens = $get_checkout_res['childrens'];
                $infants = $get_checkout_res['infants'];
                $tour_price_per_person = $get_checkout_res['tour_price_per_person'];
                $tour_total_price = $get_checkout_res['tour_total_price'];
                $created_date = $get_checkout_res['created_date'];

                $from = "admin@jomplan.com";
                $to = $get_user_res['email'];
                $user_name = $get_user_res['first_name'];

                $subject = "Jomplan - Your Tour(".$tour_name.") Booking Confirmed.";
                $headers = "From: Jomplan - Smarter Way To Travel<" . $from . "> \r\n";
                $headers .= "Return-Path: " . $from . " \r\n";
                $headers .= "Reply-To: " . $from . " \r\n";
                $headers .= "MIME-Version: 1.0\r\n";
                $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
                $headers .= "X-Priority: 3\r\n";
                $headers .= "X-Mailer: PHP" . phpversion() . "\r\n";

                $message = "<html>"
                        . "<head></head>"
                        . "<body>"
                        . "<h3>Hello " . $user_name . ",</h3><br>Your tour(<b>".$tour_name."</b>) booking confirmed, your booking reference number <b>".$reference_no."</b>. Please find the following details:<br><br>"
                        . "<table cellpadding='5' cellspacing='0' border='1' width='80%'>
                            <tr><th style='text-align:left'>Tour Name</th><td>".$tour_name."</td></tr>
                            <tr><th style='text-align:left'>Flights</th><td>".$flights_trip."</td></tr>
                            <tr><th style='text-align:left'>Depature Date</th><td>".date('M d, Y', strtotime($depature_date))."</td></tr>
                            <tr><th style='text-align:left'>Return Date</th><td>".date('M d, Y', strtotime($return_date))."</td></tr>
                            <tr><th style='text-align:left'>Adults</th><td>".$adults."</td></tr>
                            <tr><th style='text-align:left'>Teens</th><td>".$teens."</td></tr>
                            <tr><th style='text-align:left'>Childrens</th><td>".$childrens."</td></tr>
                            <tr><th style='text-align:left'>Infants</th><td>".$infants."</td></tr>
                            <tr><th style='text-align:left'>Tour Price/Person  </th><td>$".$tour_price_per_person."</td></tr>
                            <tr><th style='text-align:left'>Total Price </th><td><b>$".$tour_total_price."</b></td></tr>
                            <tr><th style='text-align:left'>Tour Booked Date </th><td><b>".date('M d, Y', strtotime($created_date))."</b></td></tr>
                            </table>"
                        . "<br><br><p>Regards,<br>Jomplan - Smarter Way To Travel</p>"
                        . "</body>"
                        . "</html>";
             
                mail($to, $subject, $message, $headers);
            }


            $json = array("status" => 1, "msg" => $suc_message);
            header('Content-type: application/json');
            echo json_encode($json);
        } else {
            $json = array("status" => 0, "msg" => "Error Occured");
            header('Content-type: application/json');
            echo json_encode($json);
        }

    } else {
        $json = array("status" => 0, "msg" => "Parameter(s) Missing!");
        header('Content-type: application/json');
        echo json_encode($json);
    }
} else {

    $json = array("status" => 0, "msg" => "Network Error");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>