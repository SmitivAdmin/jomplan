<?php
include('include/config.php');

if ($con) {
    if (($_POST["tour_id"] != "")) {

        $tour_id = $_POST["tour_id"];
        
        $reviews = array();

        
        $sql_review = "SELECT * FROM wpny_comments WHERE comment_post_ID = '$tour_id' and comment_approved = '1'";
        $qr3 =  mysqli_query($con, $sql_review);
        
        if($qr3 ->num_rows > 0)  {          
              while($row_3 = mysqli_fetch_assoc($qr3)) {     
                  
            $comment_id = $row_3["comment_ID"];
            $sql_rating = "SELECT * FROM wpny_commentmeta WHERE comment_id = '$comment_id' and meta_key = 'rating'";
            $qr4 = mysqli_query($con, $sql_rating);
             
             $rating = "";
             
               $row_4 = mysqli_fetch_array($qr4);
               $rating = $row_4["meta_value"];
               $total_rating += $row_4["meta_value"];
               
               

                
                $reviews[] = array(
                'comment_ID' => $row_3["comment_ID"],
                'comment_post_ID' => $row_3["comment_post_ID"],
                'comment_author' => $row_3["comment_author"],
                'comment_author_email' => $row_3["comment_author_email"],
                'comment_author_url' => $row_3["comment_author_url"],
                'comment_author_IP' => $row_3["comment_author_IP"],
                'comment_date' => $row_3["comment_date"],
                'comment_date_gmt' => $row_3["comment_date_gmt"],
                'comment_content' => $row_3["comment_content"],
                'comment_karma' => $row_3["comment_karma"],
                'comment_approved' => $row_3["comment_approved"],
                'comment_agent' => $row_3["comment_agent"],
                'comment_type' => $row_3["comment_type"],
                'comment_parent' => $row_3["comment_parent"],
                'comment_rating' => $rating,
                'user_id' => $row_3["user_id"]);
                    }
                 }

        //$query = mysqli_query($con, "select * from tour_details where id = '".$tour_id."' order by check_in_date asc");
         $sql_str = "SELECT wpny_posts.* FROM wpny_posts LEFT JOIN wpny_term_relationships ON wpny_term_relationships.object_id = wpny_posts.ID LEFT JOIN wpny_term_taxonomy ON wpny_term_taxonomy.term_taxonomy_id = wpny_term_relationships.term_taxonomy_id LEFT JOIN wpny_terms ON wpny_terms.term_id = wpny_term_relationships.term_taxonomy_id WHERE wpny_posts.ID = '".$tour_id."' ";
        $query = mysqli_query($con, $sql_str);

        $num_rows = mysqli_num_rows($query);
        $rows = mysqli_fetch_array($query);
        

        $dest_sql_str = "SELECT wpny_terms.name as destination FROM wpny_terms LEFT JOIN wpny_term_taxonomy on wpny_term_taxonomy.term_id = wpny_terms.term_id LEFT JOIN wpny_term_relationships on wpny_term_relationships.term_taxonomy_id = wpny_term_taxonomy.term_id WHERE wpny_term_relationships.object_id = '".$tour_id."' AND wpny_term_taxonomy.taxonomy = 'pa_destination'";
        $dest_query = mysqli_query($con, $dest_sql_str);
        $destination_array = array();
        while($dest_rows = mysqli_fetch_array($dest_query)){
            $destination_array[] = $dest_rows['destination'];
        }
        $destination = implode(', ',$destination_array);
        
         $traveltype_sql_str = "SELECT wpt.name as tourtype FROM wpny_term_taxonomy as wptt LEFT JOIN wpny_terms as wpt ON wptt.term_id = wpt.term_id LEFT JOIN wpny_term_relationships on wpny_term_relationships.term_taxonomy_id = wptt.term_id WHERE wpny_term_relationships.object_id = '".$tour_id."' AND wptt.taxonomy = 'tour_phys'";
            $tourtype_query = mysqli_query($con, $traveltype_sql_str);
            $tourtype_array = array();
            while($tourtype_rows = mysqli_fetch_array($tourtype_query)){
                $tourtype_array[] = $tourtype_rows['tourtype'];
            }
            $tourtype = implode(', ',$tourtype_array);

        $post_meta_sql_str = "SELECT
            pm1.meta_value AS tour_code,
            pm2.meta_value AS regular_price,
            pm3.meta_value AS sale_price,
            pm4.meta_value AS price,
            pm5.meta_value AS tour_duration,
            pm6.meta_value AS start_date,
            pm7.meta_value AS end_date,
            pm8.meta_value AS minimum_person,
            pm9.meta_value AS thumbnail_id,
            pm10.meta_value AS destination,
            pm11.meta_value AS tour_latitude,
            pm12.meta_value AS tour_longitude,
            pm13.meta_value AS tour_itenary
            FROM wpny_posts p
            LEFT JOIN wpny_postmeta pm1 ON (pm1.post_id = p.ID AND pm1.meta_key = '_tour_code')
            LEFT JOIN wpny_postmeta pm2 ON (pm2.post_id = p.ID AND pm2.meta_key = '_regular_price')
            LEFT JOIN wpny_postmeta pm3 ON (pm3.post_id = p.ID AND pm3.meta_key = '_sale_price')
            LEFT JOIN wpny_postmeta pm4 ON (pm4.post_id = p.ID AND pm4.meta_key = '_price')
            LEFT JOIN wpny_postmeta pm5 ON (pm5.post_id = p.ID AND pm5.meta_key = '_tour_duration')
            LEFT JOIN wpny_postmeta pm6 ON (pm6.post_id = p.ID AND pm6.meta_key = '_tour_start_date')
            LEFT JOIN wpny_postmeta pm7 ON (pm7.post_id = p.ID AND pm7.meta_key = '_tour_end_date')
            LEFT JOIN wpny_postmeta pm8 ON (pm8.post_id = p.ID AND pm8.meta_key = '_tour_number_ticket')  
            LEFT JOIN wpny_postmeta pm9 ON (pm9.post_id = p.ID AND pm9.meta_key = '_thumbnail_id') 
            LEFT JOIN wpny_postmeta pm10 ON (pm10.post_id = p.ID AND pm10.meta_key = '_tour_location_address') 
            LEFT JOIN wpny_postmeta pm11 ON (pm11.post_id = p.ID AND pm11.meta_key = '_tour_location_lat') 
            LEFT JOIN wpny_postmeta pm12 ON (pm12.post_id = p.ID AND pm12.meta_key = '_tour_location_long')
            LEFT JOIN wpny_postmeta pm13 ON (pm13.post_id = p.ID AND pm13.meta_key = '_tour_content_itinerary')    
            WHERE p.ID  = '".$tour_id."' ";

        $post_meta_qry = mysqli_query($con, $post_meta_sql_str);
        $post_meta_res = mysqli_fetch_array($post_meta_qry);

        $image_queryMeta=mysqli_query($con,"SELECT * FROM wpny_postmeta WHERE meta_key='_wp_attached_file' AND post_id='".$post_meta_res['thumbnail_id']."'");
        $image_res = mysqli_fetch_array($image_queryMeta);
        
        $tour_name = $rows['post_title'];
        $description = json_encode(utf8_encode(stripslashes($rows['post_content']))); 
        $tour_description = strip_tags(json_decode($description));
       // $tour_description = json_decode($description);
        //echo $result = '{"array":{"short_desc":"'.json_encode(utf8_encode($rows['post_excerpt'])).'"}}';

        $short_desc = json_encode(utf8_encode(stripslashes($rows['post_excerpt'])));
        $tour_short_desc = strip_tags(json_decode($short_desc));

        $itenary = json_encode(utf8_encode(stripslashes($post_meta_res['tour_itenary'])));
        $tour_itenary = strip_tags(json_decode($itenary));

        $tour_image = $product_image_url.$image_res['meta_value'];       
        $minimum_person = $post_meta_res['minimum_person'];
        $tour_price = $post_meta_res['price'];
        $regular_price = $post_meta_res['regular_price'];
        // $tour_price = number_format($post_meta_res['price'],2).'$';
        //$destination = $post_meta_res['destination'];
        $duration = $post_meta_res['tour_duration'];
        $check_in_date = date('d/m/Y', strtotime($post_meta_res['start_date']));
        $check_out_date = date('d/m/Y', strtotime($post_meta_res['end_date']));
        $tour_latitude = $post_meta_res['tour_latitude'];
        $tour_longitude = $post_meta_res['tour_longitude'];
        
        /*$parking_facility = stripcslashes($rows['parking_facility']);
        $internet_facility = stripcslashes($rows['internet_facility']);
        $food_drinks = explode(',',$rows['food_drinks']);

        $food_drinks_val = array();
        foreach ($food_drinks as $food_drink_val) {
            $food_drinks_val[] = array('item' => $food_drink_val);
        }*/
        $user_count = $qr3 ->num_rows;
			$total_rating_avg= $total_rating/$user_count; 


        $images = array();

        $images[] = array('id' => $post_meta_res['thumbnail_id'],'photo_parent' => $tour_id,'photo_img' => $tour_image);
        $sql_img_id = "select meta_value FROM wpny_postmeta WHERE post_id = '$tour_id' and meta_key = '_product_image_gallery'";
        $qry_1 = mysqli_query($con,$sql_img_id);
        if($qry_1 -> num_rows > 0) {
            $row_img_id = mysqli_fetch_array($qry_1);
            $explode_img_ids = explode(',', $row_img_id['meta_value']);

            foreach ($explode_img_ids as $key => $explode_img_id) {
                $sql_img = "select meta_value FROM wpny_postmeta WHERE post_id = '$explode_img_id' and meta_key = '_wp_attached_file'";
                $qry_2 = mysqli_query($con,$sql_img);
                $row_img = mysqli_fetch_assoc($qry_2);
                $photo_img = $product_image_url.$row_img['meta_value'];

                $images[] = array('id' => $explode_img_id,'photo_parent' => $tour_id,'photo_img' => $photo_img);
            }
        }

        /*$json = array("status" => 1, "msg" => "Tour Details", "tour_id" => $tour_id, "tour_name" => $tour_name, "tour_image" => $tour_image, "minimum_person" => $minimum_person, 
        "tour_price" => $tour_price, "regular_price" => $regular_price, "destination" => $destination,"tour_types" => $tourtype ,"duration" => $duration,
         "check_in_date" => $check_in_date, "check_out_date" => $check_out_date, "tour_description"=>$tour_description, "tour_short_desc"=>$tour_short_desc,
         "tour_itenary"=>$tour_itenary,"tour_latitude"=>$tour_latitude,"tour_longitude"=>$tour_longitude,"total_rating"=>number_format($total_rating_avg,1), "Reviews" => $reviews, "Images" => $images);*/

        // echo preg_replace('/[^0-9\.]/', '', $tour_latitude)."***".preg_replace('/[^0-9\.]/', '', $tour_longitude);

        $json = array("status" => 1, "msg" => "Tour Details", "tour_id" => $tour_id, "tour_name" => $tour_name, "tour_image" => $tour_image, "minimum_person" => $minimum_person, "tour_price" => $tour_price, "regular_price" => $regular_price, "destination" => $destination,"tour_types" => $tourtype ,"duration" => $duration, "check_in_date" => $check_in_date, "check_out_date" => $check_out_date, "tour_description"=>$tour_description, "tour_short_desc"=>$tour_short_desc, "tour_itenary"=>$tour_itenary, "tour_latitude"=>preg_replace('/[^0-9\.]/', '', $tour_latitude),"tour_longitude"=>preg_replace('/[^0-9\.]/', '', $tour_longitude), "total_rating"=>number_format($total_rating_avg,1), "Reviews" => $reviews, "Images" => $images);
        header('Content-type: application/json');
        echo json_encode($json);
        
    } else {
        $json = array("status" => 0, "msg" => "Pass tour id.");
        header('Content-type: application/json');
        echo json_encode($json);
    }
} else {

    $json = array("status" => 0, "msg" => "Network Error");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>