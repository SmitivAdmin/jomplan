<?php
include('include/config.php');
$current_url = "http://" . $_SERVER['HTTP_HOST'] . '/jomplan_webservice_new/';

if ($con) {
    if (($_POST["profile_id"] != "") && ($_POST["upload_photo_name"] != "") && ($_REQUEST["upload_photo"] != "")) {

        $profile_id = $_POST['profile_id'];
        $upload_photo_name = $_POST['upload_photo_name'];
        $image = $_REQUEST['upload_photo'];

        $data = str_replace('data:image/png;base64,', '', $image);
        $data = str_replace('data:image/jpg;base64,', '', $data);
        $data = str_replace('data:image/jpeg;base64,', '', $data);
        $data = str_replace('data:image/gif;base64,', '', $data);
        $data = str_replace(' ', '+', $data);

        $imageData = base64_decode($data);
        $imageName = 'uploads/' . time() . rand() . '.jpg';
        $source = imagecreatefromstring($imageData);
        $imageSave = imagejpeg($source, $imageName, 100);
        imagedestroy($source);
        $respon['upload_photo'] = $current_url . $imageName;

        $query = mysqli_query($con, "insert into user_submitted_photos set profile_id = '".$profile_id."', upload_photo_name = '".$upload_photo_name."', upload_photo = '".$respon['upload_photo']."' ");

        if ($query) {
            $json = array("status" => 1, "msg" => "Photo Uploaded Successfully", "profile_id" => $profile_id, "upload_photo_name" => $upload_photo_name, "upload_photo" => $respon['upload_photo']);
            header('Content-type: application/json');
            echo json_encode($json);
        } else {

            $json = array("status" => 0, "msg" => "Error Occured");
            header('Content-type: application/json');
            echo json_encode($json);
        }
        
    } else {
        $json = array("status" => 0, "msg" => "Parameter(s) Missing!");
        header('Content-type: application/json');
        echo json_encode($json);
    }
} else {

    $json = array("status" => 0, "msg" => "Network Error");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>