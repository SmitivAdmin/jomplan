<?php
include('include/config.php');

if ($con) {

    //$query = mysqli_query($con, "select * from tour_details order by check_in_date asc");
    $sql_str = "SELECT wpt.* FROM wpny_term_taxonomy as wptt INNER JOIN wpny_terms as wpt ON wptt.term_id = wpt.term_id WHERE wptt.taxonomy = 'tour_phys' order by wpt.name asc";

    $query = mysqli_query($con, $sql_str);
    $num_rows = mysqli_num_rows($query);

    if($num_rows > 0){
        $destination_list = array();
        while($rows = mysqli_fetch_array($query)){            
            $destination_id = $rows['term_id'];
            $destination_name = $rows['name'];   
            $destination_name = str_replace("&amp;","&",$destination_name);

            $destination_list[] = array("id" => $destination_id, "tour_type" => $destination_name);
        }
        $json = array("status" => 1, "msg" => "Tour types list", "tour_types_list" => $destination_list);
        header('Content-type: application/json');
        echo json_encode($json);
    } else {
        $json = array("status" => 0, "msg" => "No destinations found!");
        header('Content-type: application/json');
        echo json_encode($json);
    }
        
} else {

    $json = array("status" => 0, "msg" => "Network Error");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>