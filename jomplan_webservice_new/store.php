<?php
include('include/config.php');
if ($con) {
    if (($_POST["email_id"] != "")) {
        $email_id = $_POST["email_id"];

        $userquery = mysqli_query($con, "select * from wpny_users where user_email='".$email_id."' ");
        $user_data_rows = mysqli_fetch_array($userquery);
        $user_id = $user_data_rows['ID'];

        $booking_list = array();

        //$query = mysqli_query($con, "select td.ID, td.post_title, tc.id, tc.tour_id, tc.tour_total_price, tc.tour_status, tc.tour_date from tour_checkout as tc, wpny_posts as td where tc.profile_id='".$profile_id."' AND tc.tour_id = td.ID ");

        //echo "select pm.post_id, p.post_status from wpny_postmeta pm inner join wpny_posts p on pm.post_id = p.ID where meta_value ='".$user_id."' AND p.post_type = 'shop_order'";

        $query = mysqli_query($con, "select pm.post_id, p.post_status from wpny_postmeta pm inner join wpny_posts p on pm.post_id = p.ID where meta_value ='".$user_id."' AND p.post_type = 'shop_order' AND p.post_status = 'wc-pending'");
        while($res = mysqli_fetch_array($query)){
           
            $checkout_id = $res['post_id'];

            $ord_sql_str = "SELECT pm1.meta_value AS tour_total_price FROM wpny_posts p LEFT JOIN wpny_postmeta pm1 ON (pm1.post_id = p.ID AND pm1.meta_key = '_order_total') WHERE p.ID  = '".$checkout_id."' ";
            $ord_query = mysqli_query($con, $ord_sql_str);  
            $ord_rows = mysqli_fetch_array($ord_query);

            $order_item_meta_str = "SELECT
            om1.meta_value AS tour_id,
            om2.meta_value AS tour_date,
            om3.meta_value AS quantity,
            om4.meta_value AS sub_total_price
            FROM wpny_woocommerce_order_items o
            LEFT JOIN wpny_woocommerce_order_itemmeta om1 ON (om1.order_item_id = o.order_item_id AND om1.meta_key = '_product_id')
            LEFT JOIN wpny_woocommerce_order_itemmeta om2 ON (om2.order_item_id = o.order_item_id AND om2.meta_key = '_date_booking') 
            LEFT JOIN wpny_woocommerce_order_itemmeta om3 ON (om3.order_item_id = o.order_item_id AND om3.meta_key = '_qty') 
            LEFT JOIN wpny_woocommerce_order_itemmeta om4 ON (om4.order_item_id = o.order_item_id AND om4.meta_key = '_line_subtotal')
            WHERE o.order_id  = '".$checkout_id."' ";
            $order_item_meta_query = mysqli_query($con, $order_item_meta_str);  
            $order_item_meta_res = mysqli_fetch_array($order_item_meta_query);

            $tour_id = $order_item_meta_res['tour_id'];                       
            $tour_total_price = $ord_rows['tour_total_price'];
            $tour_date = $order_item_meta_res['tour_date'];
            $no_of_persons = $order_item_meta_res['quantity'];
            $tour_price_per_person = $order_item_meta_res['sub_total_price'] / $no_of_persons;

            $post_meta_sql_str = "SELECT p.post_title, pm1.meta_value AS thumbnail_id 
            FROM wpny_posts p 
            LEFT JOIN wpny_postmeta pm1 ON (pm1.post_id = p.ID AND pm1.meta_key = '_thumbnail_id')    
            WHERE p.ID  = '".$tour_id."' ";
            $post_meta_qry = mysqli_query($con, $post_meta_sql_str);
            $post_meta_res = mysqli_fetch_array($post_meta_qry);

            $tour_name = $post_meta_res['post_title'];

            $image_queryMeta=mysqli_query($con,"SELECT * FROM wpny_postmeta WHERE meta_key='_wp_attached_file' AND post_id='".$post_meta_res['thumbnail_id']."'");
            $image_res = mysqli_fetch_array($image_queryMeta);

            $tour_image=$product_image_url.$image_res['meta_value'];

            if($res['post_status'] == 'wc-completed'){
                $checkout_status = "Completed";
            } elseif($res['post_status'] == 'wc-processing'){
                $checkout_status = "Processing";
            } elseif($res['post_status'] == 'wc-pending'){
                $checkout_status = "Pending";
            } elseif($res['post_status'] == 'wc-on-hold'){
                $checkout_status = "On Hold";
            } elseif($res['post_status'] == 'wc-cancelled'){
                $checkout_status = "Cancelled";
            } elseif($res['post_status'] == 'wc-refunded'){
                $checkout_status = "Refunded";
            } elseif($res['post_status'] == 'wc-failed'){
                $checkout_status = "Failed";
            } 

            $booking_list[] = array("tour_id" => $tour_id, "tour_name" => $tour_name, "tour_image" => $tour_image, "checkout_id" => $checkout_id, "tour_total_price" => $tour_total_price, "tour_price_per_person" => $tour_price_per_person, "no_of_persons" => $no_of_persons, "tour_date" => date('M d, Y',strtotime($tour_date)), "checkout_status" => $checkout_status);
        }

        $json = array("status" => 1, "msg" => "My Store List", "email_id" => $email_id, "store_list" => $booking_list);
        header('Content-type: application/json');
            echo json_encode($json);

    } else {
        $json = array("status" => 0, "msg" => "Pass Profile Id.");
        header('Content-type: application/json');
        echo json_encode($json);
    }
} else {

    $json = array("status" => 0, "msg" => "Network Error");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>