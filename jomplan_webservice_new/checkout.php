<?php
include('include/config.php');
if ($con) {
    /*if (($_POST["tour_id"] != "") && ($_POST["user_email"] != "") && ($_POST["flights"] != "") && ($_POST["travel_from"] != "") && ($_POST["travel_to"] != "") && ($_POST["tour_date"] != "") && ($_POST["return_date"] != "") && ($_POST["payment_status"] != "") && ($_POST["tour_price_per_person"] != "") && ($_POST["tour_total_price"] != "") && ($_POST["passport_number"] != "") && ($_POST["passport_id"] != "")&& ($_POST["passport_name"] != "")&& ($_POST["passport_issue_date"] != "")&& ($_POST["passport_expiry_date"] != "") ) {*/

    if (($_POST["tour_id"] != "") && ($_POST["profile_id"] != "") && ($_POST["flights"] != "") && ($_POST["tour_date"] != "") && ($_POST["return_date"] != "") && ($_POST["tour_price_per_person"] != "") && ($_POST["tour_total_price"] != "")) {

        $tour_id = $_POST["tour_id"];
        $profile_id = $_POST["profile_id"];
        $flights = $_POST["flights"]; // 1 = One Way, 2 = Round Trip
        /*$travel_from = $_POST["travel_from"];
        $travel_to = $_POST["travel_to"];*/
        $tour_date = $_POST["tour_date"]; // Date = YYYY-mm-dd
        $return_date = $_POST["return_date"]; // Date = YYYY-mm-dd
        $adults = $_POST["adults"];
        $teens = $_POST["teens"];
        $childrens = $_POST["childrens"];
        $infants = $_POST["infants"];
        $tour_price_per_person = $_POST["tour_price_per_person"];
        $tour_total_price = $_POST["tour_total_price"];
/*        $passport_number=$_POST["passport_number"];
        $passport_id=$_POST["passport_id"];
        $passport_name=$_POST["passport_name"];
        $passport_issue_date=$_POST["passport_issue_date"];
        $passport_expiry_date=$_POST["passport_expiry_date"];
        $tour_status = $_POST["tour_status"];*/

        if($flights == 1){
            $flights_trip = "One Way";
        } else {
            $flights_trip = "Round Trip";
        }

        if($adults != "" || $adults != 0 || $teens != "" || $teens != 0 || $childrens != "" || $childrens != 0 || $infants != "" || $infants != 0){

            //$query = mysqli_query($con, "insert into tour_checkout set tour_id = '".$tour_id."', user_email = '".$user_email."', flights = '".$flights."', travel_from = '".$travel_from."', travel_to = '".$travel_to."', tour_date = '".$tour_date."', return_date = '".$return_date."', adults = '".$adults."', teens = '".$teens."', childrens = '".$childrens."', infants = '".$infants."', tour_status = '".$tour_status."',passport_number = '".$passport_number."',passport_id = '".$passport_id."',passport_name = '".$passport_name."',passport_issue_date = '".$passport_issue_date."',passport_expiry_date = '".$passport_expiry_date."' ");

            $query = mysqli_query($con, "insert into tour_checkout set tour_id = '".$tour_id."', profile_id = '".$profile_id."', flights = '".$flights."', tour_date = '".$tour_date."', return_date = '".$return_date."', adults = '".$adults."', teens = '".$teens."', childrens = '".$childrens."', infants = '".$infants."', tour_price_per_person = '".$tour_price_per_person."', tour_total_price = '".$tour_total_price."' ");

            if ($query) {

                $last_id = mysqli_insert_id($con);    

                $reference_no = 100000 + $last_id;
                $update_query = mysqli_query($con, "update tour_checkout set reference_no = '".$reference_no."' where id = '".$last_id."' ");

                $json = array("status" => 1, "msg" => "Payment Successful, We will send confirmation soon.", "checkout_id" => $last_id);
                header('Content-type: application/json');
                echo json_encode($json);
            } else {
                $json = array("status" => 0, "msg" => "Error Occured");
                header('Content-type: application/json');
                echo json_encode($json);
            }

        } else {

            $json = array("status" => 0, "msg" => "Any one correct value enter in travellers field.");
            header('Content-type: application/json');
            echo json_encode($json);

        }

    } else {
        $json = array("status" => 0, "msg" => "Parameter(s) Missing!");
        header('Content-type: application/json');
        echo json_encode($json);
    }
} else {

    $json = array("status" => 0, "msg" => "Network Error");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>