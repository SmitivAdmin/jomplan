<?php
include('include/config.php');
if ($con) {

        $query = mysqli_query($con, "select * from countries order by name asc");
        $country_list = array();
        while($row = mysqli_fetch_array($query)){
            $country_name = $row['name'];
            $phonecode = $row['phonecode'];
            $country_short_name = $row['sortname'];
            
            $country_list[] = array("country_name" => $country_name,"country_short_name" => $country_short_name, "phonecode"=>$phonecode);
        }    
        $json = array("status" => 1, "msg" => "Country List", "country_list" => $country_list);
        header('Content-type: application/json');
        echo json_encode($json);

} else {

    $json = array("status" => 0, "msg" => "Network Error");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>